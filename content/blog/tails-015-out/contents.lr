title: Tails 0.15 is out!
---
pub_date: 2012-11-28
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 0.15, is out.</p>

<p>All users must upgrade as soon as possible.</p>

<p><a href="https://tails.boum.org/getting_started/" rel="nofollow">Download it now</a></p>

<p>Thank you, and congrats, to everyone who helped make this happen!</p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>Tor 0.2.3.25
</li>
<li>Major new features
<ul>
<li><a href="https://tails.boum.org/doc/first_steps/persistence/configure/" rel="nofollow">Persistence for browser bookmarks</a>.</li>
<li>Support for <a href="https://tails.boum.org/doc/first_steps/startup_options/bridge_mode/" rel="nofollow">obfsproxy bridges</a>.</li>
</ul>
</li>
<li>Minor improvements
<ul>
<li>Add the Hangul (Korean) Input Method Engine for SCIM.</li>
<li>Preliminary support for some OpenPGP SmartCard readers.</li>
<li>Support printers that need HPIJS PPD and/or the IJS driver.</li>
<li>Optimize fonts display for LCD.</li>
<li>Update TrueCrypt to version 7.1a.</li>
</ul>
</li>
<li>Bugfixes
<ul>
<li>Fix gpgApplet menu display in Windows camouflage mode.</li>
<li>Fix Tor reaching an inactive state if it is restarted in "bridge mode",<br />
e.g. during the time synchronization process.</li>
</ul>
</li>
<li>Iceweasel
<ul>
<li>Update iceweasel to 10.0.11esr-1+tails1.</li>
<li>Update HTTPS Everywhere to version 3.0.4.</li>
<li>Update NoScript to version 2.6.</li>
<li>Fix bookmark to I2P router console.</li>
</ul>
</li>
<li>Localization
<ul>
<li>The Tails USB installer, tails-persistence-setup and tails-greeter<br />
are now translated into Bulgarian.</li>
<li>Update Chinese translation for tails-greeter.</li>
<li>Update Euskadi translation for WhisperBack.</li>
</ul>
</li>
</ul>

<p>Plus the usual bunch of bug reports and minor improvements.</p>

<p>See the <a href="http://git.immerda.ch/?p=amnesia.git;a=blob_plain;f=debian/changelog;hb=refs/tags/0.15" rel="nofollow">online Changelog</a> for technical details.</p>

<p>Don't hesitate to <a href="https://tails.boum.org/support/" rel="nofollow">get in touch with us</a>.</p>

