title: New Release: Tor Browser 10.5.5
---
pub_date: 2021-08-20
---
author: sysrqb
---
summary: Tor Browser 10.5.5 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 10.5.5 is now available from the <a href="https://www.torproject.org/download/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5.5/">distribution directory</a>.</p>
<p>This version updates Tor to <a href="https://blog.torproject.org/node/2062">0.4.5.10</a> that includes a fix for a security issue. On Android, this version updates Firefox to 91.2.0 and includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-33/">security updates</a>.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser will <em><b>stop</b> supporting <u>version 2 onion services</u></em> <em><b>very soon</b></em>. Please see the previously published <a href="https://blog.torproject.org/v2-deprecation-timeline">deprecation timeline</a>. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.5">Tor Browser 10.5.4</a>:</p>
<ul>
<li>All Platforms
<ul>
<li>Update Tor to 0.4.5.10</li>
</ul>
</li>
<li>Linux
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40582">Bug 40582</a>: Tor Browser 10.5.2 tabs always crash on Fedora Xfce Rawhide</li>
</ul>
</li>
<li>Android
<ul>
<li>Update Fenix to 91.2.0</li>
<li>Update NoScript to 11.2.11</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40063">Bug 40063</a>: Move custom search providers</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40176">Bug 40176</a>: TBA: sometimes I only see the banner and can't tap on the address bar</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40181">Bug 40181</a>: Remove V2 Deprecation banner on about:tor for Android</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40184">Bug 40184</a>: Rebase fenix patches to fenix v91.0.0-beta.5</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40185">Bug 40185</a>: Use NimbusDisabled</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40186">Bug 40186</a>: Hide Credit Cards in Settings</li>
</ul>
</li>
<li>Build System
<ul>
<li>Android
<ul>
<li>Update Go to 1.15.15</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40331">Bug 40331</a>: Update components for mozilla91</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292638"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292638" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Lizzz-E (not verified)</span> said:</p>
      <p class="date-time">August 20, 2021</p>
    </div>
    <a href="#comment-292638">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292638" class="permalink" rel="bookmark">I can&#039;t disable javascript…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I can't disable javascript even on the safest mode, can't visit about:config. Tor is not working with orbot. Why!!!! I was waiting for this release to resolve all this but it's the same.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292640"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292640" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 20, 2021</p>
    </div>
    <a href="#comment-292640">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292640" class="permalink" rel="bookmark">Seems like the upgrade from…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Seems like the upgrade from previous version was performed 2 times sequentially. Is it intentionally?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292643"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292643" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Davide Rossi (not verified)</span> said:</p>
      <p class="date-time">August 20, 2021</p>
    </div>
    <a href="#comment-292643">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292643" class="permalink" rel="bookmark">Somebody else have my same…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Somebody else have my same problems? in about:logins i CANT view the logins/password....<br />
Them work browsing site but in about:logins i cant view the details.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292688"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292688" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 25, 2021</p>
    </div>
    <a href="#comment-292688">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292688" class="permalink" rel="bookmark">Speaking of v3 onions,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Speaking of v3 onions, thanks for updating the built-in link to the v3 DuckDuckGo onion:</p>
<p><a href="https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion" rel="nofollow">https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion</a></p>
<p>This is also included in the version of Tor Browser in the current edition of Tails.</p>
</div>
  </div>
</article>
<!-- Comment END -->
