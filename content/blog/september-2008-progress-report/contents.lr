title: September 2008 Progress Report
---
pub_date: 2008-10-15
---
author: phobos
---
tags:

vidalia
progress report
tor browser bundle
bug fixes
stable
alpha
rpm
facebook
lectures
media articles
---
categories:

applications
releases
reports
---
_html_body:

<p><strong>Releases</strong><br />
Vidalia 0.1.9 (released September 2) fixes a big pile of bugs and inconveniences in the earlier releases. This new release marks the first "stable" release of Vidalia, in that we have now branched into a stable (0.1.x) branch and a development (0.2.x) branch.<br />
<a href="http://trac.vidalia-project.net/browser/vidalia/tags/vidalia-0.1.9/CHANGELOG" rel="nofollow">http://trac.vidalia-project.net/browser/vidalia/tags/vidalia-0.1.9/CHAN…</a></p>

<p>Tor 0.2.0.31 (released September 3) addresses two potential anonymity issues, starts to fix a big bug we're seeing where in rare cases traffic from one Tor stream gets mixed into another stream, and fixes a variety of smaller issues.<br />
<a href="http://archives.seul.org/or/announce/Sep-2008/msg00000.html" rel="nofollow">http://archives.seul.org/or/announce/Sep-2008/msg00000.html</a></p>

<p>Tor 0.2.1.6-alpha (released September 30) further improves performance and robustness of hidden services, starts work on supporting per-country relay selection, and fixes a variety of smaller issues.<br />
<a href="http://archives.seul.org/or/talk/Oct-2008/msg00093.html" rel="nofollow">http://archives.seul.org/or/talk/Oct-2008/msg00093.html</a></p>

<p><strong>Circumvention Enhancements</strong><br />
From the Vidalia 0.1.9 ChangeLog:<br />
"Correct the location of the simplified Chinese help files so they will actually load again."</p>

<p>From the Tor 0.2.1.6-alpha ChangeLog:<br />
"Start work to allow node restrictions to include country codes. The syntax to exclude nodes in a country with country code XX is "ExcludeNodes {XX}". Patch from Robert Hogan. It still needs some refinement to decide what config options should take priority if you ask to both use a particular node and exclude it."<br />
This feature should allow users in China to specify that they don't want to enter (and/or exit) in China, which in theory could provide stronger security for them.</p>

<p>From the Tor 0.2.1.6-alpha ChangeLog:<br />
"Allow ports 465 and 587 in the default exit policy again. We had rejected them in 0.1.0.15, because back in 2005 they were commonly misconfigured and ended up as spam targets. We hear they are better locked down these days."<br />
This feature lets people use GMail with Tor in more flexible ways. This approach is especially important for people trying to send email in certain configurations when their network wants to block or monitor them.</p>

<p>From the Tor 0.2.1.6-alpha ChangeLog:<br />
"Provide circuit purposes along with circuit events to the controller."<br />
This change will allow Vidalia to mark circuits in its graphical interface, so users don't get confused about why Tor is building strange circuits in the background when it's really just doing encrypted directory updates.</p>

<p>Matt and Andrew fixed a bug in the Vidalia bundle installer where it tried to detect if Firefox was installed, and unclick the "install Torbutton" option if not, but it didn't detect right. Now if Firefox is missing we put up a warning explanation about how you really ought to be using Tor with Firefox.</p>

<p>We also finally started working on a fix for the Vidalia bug where if Vidalia launches Tor and then crashes later, when you start Vidalia again it'll cryptically ask for your control password.<br />
<a href="https://wiki.torproject.org/noreply/TheOnionRouter/TorFAQ#TorPasswordPrompt" rel="nofollow">https://wiki.torproject.org/noreply/TheOnionRouter/TorFAQ#TorPasswordPr…</a><br />
The first fix is to add a "reset" button to the cryptic message, that kills Tor for you and restarts it, and a "help" button that explains what's going on. These will be out in the next development Vidalia release, hopefully in October.</p>

<p>Camilo Viecco submitted a patch for our RPM spec (build) file to let us build Red Hat / SuSE packages for 64-bit architectures. Andrew included these patches in 0.2.1.6-alpha.</p>

<p><strong>Advocacy</strong><br />
Steven Murdoch taught a lecture at the FIDIS/IFIP Brno Summer School in the Czech Republic.<br />
<a href="http://www.buslab.org/SummerSchool2008/" rel="nofollow">http://www.buslab.org/SummerSchool2008/</a><br />
The presentation was on anti-censorship in general especially on Tor.  The students seemed to be interested so he encouraged them to look at Tor and see if there is anything they'd like to work on. We will see if anything comes from that.</p>

<p>We've also been discussing creating a Facebook application, for allowing relay operators to show off that they are running a Tor relay and hopefully encourage more to do so. We think this is a good enough idea to try building it, so Steven has started to do so. As well as adding bling to a user's profile, it would also allow us to map the network of node operators. This is one of the more promising research fields to resist Sybil attacks, see e.g.<br />
"A Sybil-proof one-hop DHT, Chris Lesniewski-Laas"<br />
<a href="http://pdos.csail.mit.edu/papers/sybil-dht-socialnets08.pdf" rel="nofollow">http://pdos.csail.mit.edu/papers/sybil-dht-socialnets08.pdf</a></p>

<p>Steven had a related story regarding host-based security from his trainings in Kyrgyzstan and Poland. See also<br />
<a href="http://www.f-secure.com/weblog/archives/00001494.html" rel="nofollow">http://www.f-secure.com/weblog/archives/00001494.html</a></p>

<p>Jacob was in a story by Declan about Internet Traceback plans:<br />
"The Chinese Government, the NSA, Verisign and the ITU are getting together to trace users"<br />
<a href="http://news.cnet.com/8301-13578_3-10040152-38.html" rel="nofollow">http://news.cnet.com/8301-13578_3-10040152-38.html</a></p>

<p>The current issue of Make Magazine has an article on how to use Tor:<br />
<a href="http://www.make-digital.com/make/vol15/?pg=102" rel="nofollow">http://www.make-digital.com/make/vol15/?pg=102</a></p>

<p>Helped Kasimir add new Tor controller features so Torstatus can switch to using the v3 directory system:<br />
<a href="http://trunk.torstatus.kgprog.com/" rel="nofollow">http://trunk.torstatus.kgprog.com/</a></p>

<p><strong>Ease of Use</strong><br />
Steven is working on a new branch of Vidalia that can be used in Tor Browser Bundle, for launching Firefox directly without needing the extra installer scripts called "Firefox Portable". If we get this working, then we can hopefully make progress on running multiple Firefoxes at once (one used for Tor launched by TBB, and one used for non-Tor).<br />
<a href="http://trac.vidalia-project.net/browser/vidalia/branches/alt-launcher" rel="nofollow">http://trac.vidalia-project.net/browser/vidalia/branches/alt-launcher</a></p>

<p>Jacob Appelbaum worked on a set of instructions for rebranding Firefox, if we decide that we need to call the browser that ships in the Tor Browser Bundle something other than "Firefox". The instructions aren't complete, for example because we need more replacement logos.<br />
<a href="https://svn.torproject.org/svn/torbrowser/trunk/build-scripts/branding/" rel="nofollow">https://svn.torproject.org/svn/torbrowser/trunk/build-scripts/branding/</a><br />
It looks like the process of rebranding Firefox 3 is much more straightforward. We have "move to FF3" on our TBB roadmap.</p>

<p>Work by Martin and Kyle on the Tor VM project continues. We have a very early prototype available now:<br />
<a href="http://peertech.org/files/demo/testinfo.html" rel="nofollow">http://peertech.org/files/demo/testinfo.html</a><br />
and we hope to give it some more testing and better documentation in the coming months.</p>

<p><strong>Scalability</strong><br />
Joel Reardon, Ian Goldberg's student at Waterloo, has finished the final version of his thesis "Improving Tor using a TCP-over-DTLS tunnel":<br />
<a href="http://uwspace.uwaterloo.ca/handle/10012/4011" rel="nofollow">http://uwspace.uwaterloo.ca/handle/10012/4011</a><br />
We funded this research (along with 4x matching funding from MITACS in Canada) in the hopes that it would move us close enough to being able to switch to a UDP design that we can put it on the Tor development roadmap at some point. Many large challenges remain, but this is also promising work in that it shows that we can expect very serious performance improvements if we go this route.</p>

<p>We've started hunting more thoroughly for solutions to Bug 676:<br />
<a href="https://bugs.torproject.org/flyspray/index.php?do=details&amp;id=696" rel="nofollow">https://bugs.torproject.org/flyspray/index.php?do=details&amp;id=696</a><br />
The issue is that some of the v3 directory authorities are keeping bad statistics on uptimes and stability of relays, which means they are not assigning the Stable or Guard flag correctly to them. The result is that the networkstatus consensus mislabels them, and clients end up not choosing relays or circuits in an efficient manners. This bug not only results in bad performance for clients, but also results in overloading some relays, leading to worse performance.</p>

<p>From the Tor 0.2.1.6-alpha ChangeLog:<br />
"Implement most of Proposal 152: allow specialized servers to permit single-hop circuits, and clients to use those servers to build single-hop circuits when using a specialized controller. Patch from Josh Albrecht. Resolves feature request 768."<br />
"Fixed some memory leaks -- some quite frequent, some almost impossible to trigger -- based on results from Coverity."</p>

<p>Several security- and integrity-related bugfixes from Tor 0.2.0.31:<br />
"Make sure that two circuits can never exist on the same connection with the same circuit ID, even if one is marked for close. This is conceivably a bugfix for bug 779. Bugfix on 0.1.0.4-rc."<br />
"Relays now reject risky extend cells: if the extend cell includes a digest of all zeroes, or asks to extend back to the relay that sent the extend cell, tear down the circuit. Ideas suggested by rovv."<br />
"If not enough of our entry guards are available so we add a new one, we might use the new one even if it overlapped with the current circuit's exit relay (or its family). Anonymity bugfix pointed out by rovv."</p>

---
_comments:

<a id="comment-405"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-405" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 25, 2008</p>
    </div>
    <a href="#comment-405">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-405" class="permalink" rel="bookmark">Interested in joining and contributing to TOR</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've come across TOR before and liked the idea - I understand the basic principle and see how the basic concept works as a security measure.  I didn't do much at the time but now I'm looking for a safe way to surf and use the Internet.</p>
<p>However, having read your pages, I can't make sense of what it's all about and there seem to be an awful lot of settings that mean nothing to me.</p>
<p>Can you explain to me, in relatively simple terms what joining TOR means, what sort of security it will afford me, am I expecting more in terms of security than it is really capable of providing and, if it is the security measure that I'm looking for, how do I go about getting it onto my computer (MAC Leopard) so that it works and I can surf and use the internet without the nosey buggers seeing what I am up to.</p>
<p>I'd appreciate your thoughts.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-417"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-417" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">December 01, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-405" class="permalink" rel="bookmark">Interested in joining and contributing to TOR</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-417">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-417" class="permalink" rel="bookmark">There is no &quot;joining&quot; per</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There is no "joining" per se.  It's a free software program that anyone can use.  You can read more about Tor at <a href="https://www.torproject.org/30seconds.html.en" rel="nofollow">https://www.torproject.org/30seconds.html.en</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-508"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-508" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anon Y. Mous (not verified)</span> said:</p>
      <p class="date-time">January 11, 2009</p>
    </div>
    <a href="#comment-508">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-508" class="permalink" rel="bookmark">Portable Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is the Tor Project working on a Portable Tor for USB keys?  I have seen one on the net that packages Tor, Privoxy and Vidalia with the website having some links back to The Tor Project but I do not know if it is coming from a reliable source.  I began to do an install and it did not do what the instructions said it should.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-512"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-512" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 12, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-508" class="permalink" rel="bookmark">Portable Tor</a> by <span>Anon Y. Mous (not verified)</span></p>
    <a href="#comment-512">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-512" class="permalink" rel="bookmark">Tor Browser Bundle</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We have something similar at <a href="https://www.torproject.org/torbrowser/" rel="nofollow">https://www.torproject.org/torbrowser/</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-514"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-514" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anon Y. Mous (not verified)</span> said:</p>
      <p class="date-time">January 12, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-514">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-514" class="permalink" rel="bookmark">Tor bundle</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for pointing me to the Tor bundle.</p>
<p>Two challenges:  1.  It is in Alpha;  2.  I already have Portable FF3.05.  I read the install instructions and it does not appear to include a custom install option so that I can choose which components to install (ie everything but FF).  If not, is there any reason why I cannot install the standalone versions of Tor and the other complementary apps on my key?</p>
<p>I have a lot of time invested in building my FF with add-ons and extensions and don't want to reverse everything.  Unfortunately I don't have the time at the moment to be involved in testing or I would install the alpha bundle.</p>
<p>Also, I am using the PortableApps.com platform.  Are there any plans for the TorProject to build a portable version for this platform or is that what the Tor Bundle is intended for?  I will be asking the same question on the PA forum:-)</p>
<p>Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-513"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-513" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 12, 2009</p>
    </div>
    <a href="#comment-513">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-513" class="permalink" rel="bookmark">ya any updates for the tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>ya any updates for the tor browser bundle on tat link or still the same version 2.01.8.Please tell me</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-515"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-515" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anon Y. Mous (not verified)</span> said:</p>
      <p class="date-time">January 12, 2009</p>
    </div>
    <a href="#comment-515">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-515" class="permalink" rel="bookmark">Me Bad:-(</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>OK, slap me.  For whatever reason I had not searched the PA forums.  There is lots of discussion about portable TOR although there is still not an official PA build.  So I'd still settle for installing the standalone build from here if feasable.<br />
Thanks...and I'll try to do my homework next time;-)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1476"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1476" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 04, 2009</p>
    </div>
    <a href="#comment-1476">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1476" class="permalink" rel="bookmark">RMP SUSE binary bundle for 386 only-where is the binary for 586?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>ERROR on a SUSE 11 Desktop</p>
<p>Package /root/Desktop/tor-0.2.0.34-tor.0.suse.i386.rpm could not be installed.</p>
<p>Details:<br />
Subprocess failed. Error: RPM failed: error: Failed dependencies:<br />
    libevent-1.4.so.2 is needed by tor-0.2.0.34-tor.0.suse.i586<br />
regards</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1478"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1478" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">June 04, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1476" class="permalink" rel="bookmark">RMP SUSE binary bundle for 386 only-where is the binary for 586?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1478">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1478" class="permalink" rel="bookmark">re: SUSE rpms</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The actual error there is that libevent-1.4 isn't installed, not that there isn't an i586 rpm.  i386 rpms work fine in i586 systems.  </p>
<p>You should install libevent-1.4.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-9886"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9886" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 14, 2011</p>
    </div>
    <a href="#comment-9886">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9886" class="permalink" rel="bookmark">Hello everybody,
for the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello everybody,</p>
<p>for the speed I would like to enter only the ip addresses from Holland and Belgium and I read, that I can do so, by adding ExcludeNodes in the edit torcc file for all the countries I would like to exclude.<br />
I use 02.1.30 version.</p>
<p>In my screen torcc file I see only this message:</p>
<p># This file was generated by Tor; if you edit it, comments will not be preserved<br />
# The old torrc file was renamed to torrc.orig.1 or similar, and Tor will ignore it</p>
<p># If non-zero, try to write to disk less frequently than we would otherwise.<br />
AvoidDiskWrites 1<br />
# If set, Tor will accept connections from the same machine (localhost only)<br />
# on this port, and allow those connections to control the Tor process using<br />
# the Tor Control Protocol (described in control-spec.txt).<br />
ControlPort 9051<br />
# Store working data, state, keys, and caches here.<br />
DataDirectory .\Data\Tor<br />
GeoIPFile .\Data\Tor\geoip<br />
# Where to send logging messages.  Format is minSeverity[-maxSeverity]<br />
# (stderr|stdout|syslog|file FILENAME).<br />
Log notice stdout<br />
# Bind to this address to listen to connections from SOCKS-speaking<br />
# applications.<br />
SocksListenAddress 127.0.0.1</p>
<p>Can anyone tell me where I should type the ExcludeNodes and does have anyone experience by doing this?</p>
<p>Thanks and keep up the good work,</p>
</div>
  </div>
</article>
<!-- Comment END -->
