title: This is What a Tor Supporter Looks Like: Daniel Ellsberg
---
pub_date: 2015-12-26
---
author: nickm
---
tags:

funding
crowdfunding
fundraising
---
categories: fundraising
---
_html_body:

<p><a href="https://www.torproject.org/donate/donate-blog-ellsberg" rel="nofollow"><img src="https://extra.torproject.org/blog/2015-12-26-what-tor-supporter-looks-daniel-ellsberg/this-is-what-a-ellsberg.jpg" alt="Dan Ellsberg and Patricia Marx Ellsberg, Privacy Activists" /></a></p>

<p>“The public, the media, and congress have tolerated, for the entire length of the Cold War and since, an extremely elaborate secrecy system in the government on the declared rationale that this is necessary for national security.</p>

<p>“Actually, this is largely a fraud.  Secrets can be necessary from foreign enemies, but we rarely need secrecy for more than a few years. And yet the vast amount of the billions and billions of pages of classified secret documents are…decades old. The only reason for that is to protect officials: from accountability, from blame for criminal prosecution in some cases, but more often just from mistakes, from admitting error, bad predictions, deceptions of the public, disastrous policies. In other words, these documents remain classified to protect officials from the electorate, to subvert democracy essentially. And that has been the effect.</p>

<p>“The price of maintaining this lack of responsibility and accountability was Vietnam, and the Iraq war. Had there been an Ed Snowden or a Chelsea Manning at high levels in the government in let’s say, 2001 or 2002, there would have been no Iraq war.</p>

<p>“Likewise, had I thought of putting out what I knew already in 1964 and 1965 to the press—what I did 5 years later in the Pentagon Papers—there would have been no Vietnam War.  Future disasters lie ahead unless whistleblowers do emerge who can communicate securely and safely with media and get the information out to the public.</p>

<p>“Tor permits the possibility of the continuation of whistle-blowing. That really means the continuation of investigative journalism in the area of so-called national security, and that in turn means the recapture or preservation of democracy, of the first amendment. You can’t have a democratic government with as little public information as the government now allows us. That has to change.  We need more whistle blowers—more Mannings, more Snowdens—and that is not going to happen with the government’s current capability to trace and listen to every source, every journalist, every congressional staffer, through capabilities that simply didn’t exist for the East German secret police, the Stasi.  The government now has capabilities the Stasi couldn’t even imagine, the possibility for a total authoritarian control.</p>

<p>“The counter to that is courage, because putting out information that the government doesn’t want told…will never be without risk. There has to be courage. But to limit the risks and make that more accessible we have to have the ability for secure, safe, anonymous communication, to congressional staff, to the media, to the public at large. And that is what Tor facilitates.  So I would say that the future, the future of democracy, and not only in this country, depends upon countering the abilities of this government and every other government in this world to know everything about our private lives while they keep secret everything about what they’re doing officially.”</p>

<p> — Dan Ellsberg</p>

<p><a href="https://www.torproject.org/donate/donate-blog-ellsberg" rel="nofollow">Support Tor today!</a></p>

---
_comments:

<a id="comment-143983"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143983" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 26, 2015</p>
    </div>
    <a href="#comment-143983">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143983" class="permalink" rel="bookmark">it is a very interesting</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>it is a very interesting pamphlet even if i disagree ...<br />
it is a tor fund campaign so let's support it ...<br />
thx dan ellsberg for your courage.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-143992"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143992" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 26, 2015</p>
    </div>
    <a href="#comment-143992">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143992" class="permalink" rel="bookmark">Amen.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Amen.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-144312"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-144312" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 28, 2015</p>
    </div>
    <a href="#comment-144312">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-144312" class="permalink" rel="bookmark">A million thanks to Daniel</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A million thanks to Daniel Ellsberg for his whistle-blowing, his good citizenship over the decades, and his support of Tor Project!  A true hero for our time.</p>
<p>WoT is like Vietnam war on steroids :-P</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-147176"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-147176" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 03, 2016</p>
    </div>
    <a href="#comment-147176">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-147176" class="permalink" rel="bookmark">Agreed. Thank you Mr.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Agreed. Thank you Mr. Ellsberg!</p>
</div>
  </div>
</article>
<!-- Comment END -->
