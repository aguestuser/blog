title: New Release: Tor Browser 10.5.9 (Android Only)
---
pub_date: 2021-10-11
---
author: sysrqb
---
tags:

tor browser
tbb-10.5
tbb
---
categories: applications
---
summary: Tor Browser 10.5.9 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 10.5.9 is now available from the <a href="https://www.torproject.org/download/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5.9/">distribution directory</a>.</p>
<p>This version is a bugfix for Android.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser will <em><b>stop</b> supporting <u>version 2 onion services</u></em> <em><b>very soon</b></em>. Please see the previously published <a href="https://blog.torproject.org/v2-deprecation-timeline">deprecation timeline</a>. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.5">Tor Browser 10.5.8</a>:</p>
<ul>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40052">Bug 40052</a>: Skip L10nRegistry source registration on Android</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292971"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292971" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Possibly anonymous (not verified)</span> said:</p>
      <p class="date-time">October 11, 2021</p>
    </div>
    <a href="#comment-292971">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292971" class="permalink" rel="bookmark">So what does L10nRegistry…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So what does L10nRegistry actually do? The fact you've rushed an update out makes me believe it's something critical. Does this mean everyone using the release before this has been exposing information? If so what information has been exposed and how detailed is it? It might have been an idea to check on the last update to avoid this.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-293106"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293106" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">October 25, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292971" class="permalink" rel="bookmark">So what does L10nRegistry…</a> by <span>Possibly anonymous (not verified)</span></p>
    <a href="#comment-293106">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293106" class="permalink" rel="bookmark">The L10nRegistry provides…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The L10nRegistry provides localized strings (translations) on Windows, macOS, and Linux. It is not used on Android. The previous release did not expose information. The browser's Security Level was not set correctly in that version.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292974"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292974" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 11, 2021</p>
    </div>
    <a href="#comment-292974">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292974" class="permalink" rel="bookmark">This version is a bugfix for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>This version is a bugfix for Android.</p></blockquote>
<p>Please append " (Android)" to the title of the post like the titles of the previous Android-only posts. Thank you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
