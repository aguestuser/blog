title: New Release: Tor Browser 10.5a1
---
pub_date: 2020-09-24
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-10.5
---
categories: applications
---
summary: Tor Browser 10.5a1 is now available from the Tor Browser Alpha download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 10.5a1 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5a1/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-100">latest stable release</a> instead.</p>
<p>Tor Browser 10.5a1 ships with Firefox 78.3.0esr, updates NoScript to 11.0.44, and Tor to 0.4.4.5.</p>
<p><strong>Note:</strong> Tor Browser 10.5 <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40089">does not support CentOS 6</a>.</p>
<p><strong>Note:</strong> Now Javascript on the Safest security level is <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40082">governed by NoScript</a> again. It was set as <strong>false</strong> when on <em>Safest</em> in <a href="https://blog.torproject.org/new-release-tor-browser-95a9">9.5a9</a>. The <strong>javascript.enabled</strong> preference was reset to <strong>true</strong> for everyone using <em>Safest</em> and you must re-set it as <strong>false</strong> if that is your preference.</p>
<p><strong>Note:</strong> After investigating the error seen by Windows users while playing videos on Youtube, a user <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40140#note_2709551">helped us identify</a> the cause. Until this is fixed in an upcoming release, a <strong>workaround</strong> is setting <strong>media.rdd-opus.enabled</strong> as <strong>false</strong> in <em>about:config</em>.</p>
<p>The <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">full changelog</a> since Tor Browser 10.0a7 is:</p>
<ul>
<li>Windows + OS X + Linux
<ul>
<li>Update Firefox to 78.3.0esr</li>
<li>Update Tor to 0.4.4.5</li>
<li>Update Tor Launcher to 0.2.25
<ul>
<li>Translations update</li>
</ul>
</li>
<li>Update NoScript to 11.0.44
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40093">Bug 40093</a>: Youtube videos on safer produce an error</li>
</ul>
</li>
<li>Translations update</li>
</ul>
</li>
<li>Linux
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40089">Bug 40089</a>: Remove CentOS 6 support for Tor Browser 10.5</li>
</ul>
</li>
<li>Build System
<ul>
<li>Linux
<ul>
<li><a href="https://bugs.torproject.org/26238">Bug 26238</a>: Move to Debian Jessie for our Linux builds</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40041">Bug 40041</a>: Remove CentOS 6 support for 10.5 series</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40103">Bug 40103</a>: Add i386 pkg-config path for linux-i686</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-289678"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289678" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 24, 2020</p>
    </div>
    <a href="#comment-289678">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289678" class="permalink" rel="bookmark">Is it okay to media.rdd-opus…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is it okay to media.rdd-opus.enabled as false in the stable version too?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289979"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289979" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 19, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289678" class="permalink" rel="bookmark">Is it okay to media.rdd-opus…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289979">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289979" class="permalink" rel="bookmark">Yes, a note was added to the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, a note was added to <a href="https://blog.torproject.org/new-release-tor-browser-100" rel="nofollow">the post for 10.0 stable</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289680"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289680" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 25, 2020</p>
    </div>
    <a href="#comment-289680">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289680" class="permalink" rel="bookmark">09:05:37.244 Uncaught…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>09:05:37.244 Uncaught TypeError: aSubject is null<br />
    observe chrome://browser/content/pageinfo/permissions.js:25<br />
    torbutton_do_new_identity chrome://torbutton/content/torbutton.js:917<br />
    torbutton_new_identity chrome://torbutton/content/torbutton.js:791<br />
    oncommand chrome://browser/content/browser.xhtml:1<br />
permissions.js:25:24<br />
    observe chrome://browser/content/pageinfo/permissions.js:25<br />
    torbutton_do_new_identity chrome://torbutton/content/torbutton.js:917<br />
    torbutton_new_identity chrome://torbutton/content/torbutton.js:791<br />
    oncommand chrome://browser/content/browser.xhtml:1</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289725"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289725" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Bali (not verified)</span> said:</p>
      <p class="date-time">September 27, 2020</p>
    </div>
    <a href="#comment-289725">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289725" class="permalink" rel="bookmark">Suggestion to update openssl…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Suggestion to update openssl to latest version in the next alpha. Love your work!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289726"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289726" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Baali (not verified)</span> said:</p>
      <p class="date-time">September 27, 2020</p>
    </div>
    <a href="#comment-289726">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289726" class="permalink" rel="bookmark">Hi, there is a new version…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi, there is a new version of OpenSSL available, update it for next alpha maybe?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289727"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289727" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Baali (not verified)</span> said:</p>
      <p class="date-time">September 27, 2020</p>
    </div>
    <a href="#comment-289727">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289727" class="permalink" rel="bookmark">Looks like you can&#039;t write a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Looks like you can't write a comment when having the security slider on safest... why is the site using javascript anyways? There is a new version of OpenSSL that you could update in the next Tor alpha release and maybe for the android version too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289978"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289978" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 19, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289727" class="permalink" rel="bookmark">Looks like you can&#039;t write a…</a> by <span>Baali (not verified)</span></p>
    <a href="#comment-289978">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289978" class="permalink" rel="bookmark">&gt; you can&#039;t write a comment…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; you can't write a comment when having the security slider on safest</p>
<p>Correct. That's <a href="https://trac.torproject.org/projects/tor/ticket/22530" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/22530</a><br />
I agree to make torproject.org, where the browser comes from, be usable in the browser's safest mode.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289733"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289733" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 29, 2020</p>
    </div>
    <a href="#comment-289733">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289733" class="permalink" rel="bookmark">Suggestion: To avoid…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Suggestion: To avoid possible confusion, the first sentence in the text body of this blog notice should read "Tor Browser 10.5a1 is now available from the Tor Browser Alpha download page and also from our distribution directory," instead of "Tor Browser 10.1a1 is now available from the Tor Browser Alpha download page and also from our distribution directory."</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289958"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289958" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">October 19, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289733" class="permalink" rel="bookmark">Suggestion: To avoid…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289958">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289958" class="permalink" rel="bookmark">Thanks!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289757"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289757" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 02, 2020</p>
    </div>
    <a href="#comment-289757">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289757" class="permalink" rel="bookmark">Cannot update to 10.5a1 from…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Cannot update to 10.5a1 from 10.0a4.</p>
<p>10.0a4 (based on Mozilla Firefox 68.11.0esr) (32-bit)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289959"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289959" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">October 19, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289757" class="permalink" rel="bookmark">Cannot update to 10.5a1 from…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289959">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289959" class="permalink" rel="bookmark">What is the error or problem…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What is the error or problem you see?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289960"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289960" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">October 19, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-289960">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289960" class="permalink" rel="bookmark">You may be experiencing the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You may be experiencing the problem described in the Note at the bottom of this blog post:<br />
<a href="https://blog.torproject.org/new-release-tor-browser-100a7" rel="nofollow">https://blog.torproject.org/new-release-tor-browser-100a7</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-289803"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289803" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 06, 2020</p>
    </div>
    <a href="#comment-289803">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289803" class="permalink" rel="bookmark">Click in the dialog to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Click in the dialog to download but the installation fails.<br />
Can not update to 10.5a1.</p>
<p>Tor Browser 10.0a4<br />
10.0a4 (based on Mozilla Firefox 68.11.0esr) (32-bit)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289992"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289992" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 20, 2020</p>
    </div>
    <a href="#comment-289992">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289992" class="permalink" rel="bookmark">SecureDropTorOnion: Could…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>SecureDropTorOnion: Could not import key.  Aborting. util.js:24:15</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289993"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289993" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">October 20, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289992" class="permalink" rel="bookmark">SecureDropTorOnion: Could…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289993">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289993" class="permalink" rel="bookmark">Were there any other errors…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Were there any other errors or messages around this? Do you see this after restarting the browser, too?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
