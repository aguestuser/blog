title: Tor 0.2.2.20-alpha is out (security patches)
---
pub_date: 2010-12-20
---
author: phobos
---
tags:

security advisory
bug fixes
enhancements
---
categories: releases
---
_html_body:

<p>Tor 0.2.2.20-alpha does some code cleanup to reduce the risk of remotely<br />
exploitable bugs. Thanks to Willem Pinckaers for notifying us of the<br />
issue. The Common Vulnerabilities and Exposures project has assigned<br />
CVE-2010-1676 to this issue.</p>

<p>We also fix a variety of other significant bugs, change the IP address<br />
for one of our directory authorities, and update the minimum version<br />
that Tor relays must run to join the network.</p>

<p>All Tor users should upgrade.</p>

<p><a href="https://www.torproject.org/download/download" rel="nofollow">https://www.torproject.org/download/download</a></p>

<p>Changes in version 0.2.2.20-alpha - 2010-12-17<br />
<strong>Major bugfixes:</strong></p>

<ul>
<li>Fix a remotely exploitable bug that could be used to crash instances<br />
      of Tor remotely by overflowing on the heap. Remote-code execution<br />
      hasn't been confirmed, but can't be ruled out. Everyone should<br />
      upgrade. Bugfix on the 0.1.1 series and later.</li>
<li>Fix a bug that could break accounting on 64-bit systems with large<br />
      time_t values, making them hibernate for impossibly long intervals.<br />
      Fixes bug 2146. Bugfix on 0.0.9pre6; fix by boboper.</li>
<li>Fix a logic error in directory_fetches_from_authorities() that<br />
      would cause all _non_-exits refusing single-hop-like circuits<br />
      to fetch from authorities, when we wanted to have _exits_ fetch<br />
      from authorities. Fixes more of 2097. Bugfix on 0.2.2.16-alpha;<br />
      fix by boboper.</li>
<li>Fix a stream fairness bug that would cause newer streams on a given<br />
      circuit to get preference when reading bytes from the origin or<br />
      destination. Fixes bug 2210. Fix by Mashael AlSabah. This bug was<br />
      introduced before the first Tor release, in svn revision r152.</li>
</ul>

<p><strong>Directory authority changes:</strong></p>

<ul>
<li>Change IP address and ports for gabelmoo (v3 directory authority).</li>
</ul>

<p><strong>Minor bugfixes:</strong></p>

<ul>
<li>Avoid crashes when AccountingMax is set on clients. Fixes bug 2235.<br />
      Bugfix on 0.2.2.18-alpha. Diagnosed by boboper.</li>
<li>Fix an off-by-one error in calculating some controller command<br />
      argument lengths. Fortunately, this mistake is harmless since<br />
      the controller code does redundant NUL termination too. Found by<br />
      boboper. Bugfix on 0.1.1.1-alpha.</li>
<li>Do not dereference NULL if a bridge fails to build its<br />
      extra-info descriptor. Found by an anonymous commenter on<br />
      Trac. Bugfix on 0.2.2.19-alpha.</li>
</ul>

<p><strong>Minor features:</strong></p>

<ul>
<li>Update to the December 1 2010 Maxmind GeoLite Country database.
</li>
<li>Directory authorities now reject relays running any versions of<br />
      Tor between 0.2.1.3-alpha and 0.2.1.18 inclusive; they have<br />
      known bugs that keep RELAY_EARLY cells from working on rendezvous<br />
      circuits. Followup to fix for bug 2081.</li>
<li>Directory authorities now reject relays running any version of Tor<br />
      older than 0.2.0.26-rc. That version is the earliest that fetches<br />
      current directory information correctly. Fixes bug 2156.</li>
<li>Report only the top 10 ports in exit-port stats in order not to<br />
      exceed the maximum extra-info descriptor length of 50 KB. Implements<br />
      task 2196.</li>
</ul>

