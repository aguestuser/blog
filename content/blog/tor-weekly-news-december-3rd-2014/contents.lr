title: Tor Weekly News — December 3rd, 2014
---
pub_date: 2014-12-03
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the forty-eighth issue in 2014 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>GetTor is back</h1>

<p>Some Tor users need to access the Internet from networks so heavily censored that they cannot reach the Tor Project website, or any of its mirrors, to download Tor in the first place; with these users in mind, <a href="https://www.torproject.org/projects/gettor" rel="nofollow">GetTor</a>, an alternative software distribution system for Tor Browser, was created.</p>

<p>After a period of neglect, GetTor has been revamped and redeployed: users can now email the name of their operating system to <a href="mailto:gettor@torproject.org" rel="nofollow">gettor@torproject.org</a>, and in return they will receive Dropbox download links for the latest Tor Browser and the package signature, as well as a checksum and the fingerprint of the key used to make the signature.</p>

<p>The lead developer on this project is Israel Leiva, who did most of the work on it during this year’s Google Summer of Code. Israel took to the <a href="https://blog.torproject.org/blog/say-hi-new-gettor" rel="nofollow">Tor blog</a> to explain the background and outcome of the redevelopment work; please see that post for more information, or put GetTor to the test yourself and send your comments to the community!</p>

<h1>Monthly status reports for November 2014</h1>

<p>The wave of regular monthly reports from Tor project members for the month of November has begun. <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000698.html" rel="nofollow">Damian Johnson</a> released his report first, followed by reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000699.html" rel="nofollow">Juha Nurmi</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000701.html" rel="nofollow">George Kadianakis</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000702.html" rel="nofollow">David Goulet</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000703.html" rel="nofollow">Philipp Winter</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000704.html" rel="nofollow">Sherief Alaa</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000705.html" rel="nofollow">Tom Ritter</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000706.html" rel="nofollow">Nick Mathewson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000708.html" rel="nofollow">Georg Koppen</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000709.html" rel="nofollow">Griffin Boyce</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000710.html" rel="nofollow">Karsten Loesing</a>, Andrew Lewman (for both <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000712.html" rel="nofollow">October</a> and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000713.html" rel="nofollow">November</a>), <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000714.html" rel="nofollow">Noel Torres</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000717.html" rel="nofollow">Harmony</a>.</p>

<p>George Kadianakis also sent out the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000700.html" rel="nofollow">SponsorR report</a>, while Colin C. reported on behalf of the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000711.html" rel="nofollow">help desk</a>, and Mike Perry for the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000716.html" rel="nofollow">Tor Browser team</a>.</p>

<h1>Miscellaneous news</h1>

<p>Nathan Freitas <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2014-November/004080.html" rel="nofollow">announced</a> version 14.1.4 of Orbot, the Tor client for Android, which brings with it further improvements to background service operation, as well as theme and layout tweaks.</p>

<p>After much back-and-forth, work by Andrea Shepard to make Tor’s cell scheduling mechanism more efficient was finally <a href="https://bugs.torproject.org/9262" rel="nofollow">merged</a>. Although performance is not yet affected, these changes could form the basis of other improvements to managing congestion caused by “mismanaged socket output” in the Tor network, as discussed by Jansen et al. in “<a href="http://www.robgjansen.com/publications/kist-sec2014.pdf" rel="nofollow">Never Been KIST</a>”.</p>

<p>Following a discussion with David Goulet, Nick Mathewson posted a <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/007898.html" rel="nofollow">draft proposal</a> of possible improvements to integration testing for Tor.</p>

<p>Sebastian Hahn <a href="https://lists.torproject.org/pipermail/tor-dev/2014-November/007892.html" rel="nofollow">informed</a> users of the Tor Project’s git repositories that cloning via the unauthenticated git:// protocol is no longer supported — secure https:// access has been and still is the preferred method for retrieving code.</p>

<p>Gareth Owen started a <a href="https://lists.torproject.org/pipermail/tor-dev/2014-November/007870.html" rel="nofollow">discussion</a> of suspicious relay behaviors that automated Tor network tests could scan for, in addition to those that are already monitored.</p>

<h1>Tor help desk roundup</h1>

<p>The help desk has been asked how to set up a relay on a Windows laptop. We don’t recommend running a relay on a laptop: the relays that are most useful to the network have faster bandwidth than most home internet connections can offer. Relays also need to have as much uptime as possible, and a laptop that gets put to sleep and woken up once a week or more is not a good computing environment for a relay that should serve the network in a consistent way.</p>

<p>We are not able to provide much help to users who report errors when using any of the Vidalia bundles, as Vidalia is no longer maintained.</p>

<p>This issue of Tor Weekly News has been assembled by Matt Pagan, Nick Mathewson, Roger Dingledine, and Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

