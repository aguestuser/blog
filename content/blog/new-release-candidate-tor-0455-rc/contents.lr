title: New release candidate: Tor 0.4.5.5-rc
---
pub_date: 2021-02-01
---
author: nickm
---
_html_body:

<p>There's a new release candidate available for download. If you build Tor from source, you can download the source code for 0.4.5.5-rc from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release in the coming week.</p>
<p>We think this is almost ready to be stable: please test it out, and let us know on the bugtracker if there are any major problems!</p>
<p>Tor 0.4.5.5-rc is the third release candidate in its series. We're coming closer and closer to a stable release series. This release fixes an annoyance with address detection code, and somewhat mitigates an ongoing denial-of-service attack.</p>
<p>We anticipate no more code changes between this and the stable release, though of course that could change.</p>
<h2>Changes in version 0.4.5.5-rc - 2021-02-01</h2>
<ul>
<li>Major feature (exit):
<ul>
<li>Re-entry into the network is now denied at the Exit level to all relays' ORPorts and authorities' ORPorts and DirPorts. This change should help mitgate a set of denial-of-service attacks. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/2667">2667</a>.</li>
</ul>
</li>
<li>Minor bugfixes (relay, configuration):
<ul>
<li>Don't attempt to discover our address (IPv4 or IPv6) if no ORPort for it can be found in the configuration. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40254">40254</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-291122"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291122" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>BeeFi (not verified)</span> said:</p>
      <p class="date-time">February 10, 2021</p>
    </div>
    <a href="#comment-291122">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291122" class="permalink" rel="bookmark">I had major problem with…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I had major problem with this on Debian Buster. My relays torrc have:<br />
ORPort {localIP behind NAT}:443 NoAdvertise<br />
ORPort {externalIP}:443 NoListen</p>
<p>After upgrade to 0.4.5.5-rc, Tor does not even start and error says, that there is not any ORPort to listen, even if one is advertised. I did not have any problem with my relay or torrc config before, so I had to downgrade. </p>
<p>Someone else had similar problem: <a href="https://www.reddit.com/r/TOR/comments/lgnt7g/orport_is_not_reachable_after_updating_tor_from/" rel="nofollow">https://www.reddit.com/r/TOR/comments/lgnt7g/orport_is_not_reachable_af…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
