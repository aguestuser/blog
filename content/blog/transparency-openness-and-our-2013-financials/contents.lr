title: Transparency, Openness, and our 2013 Financials
---
pub_date: 2014-07-26
---
author: phobos
---
tags:

privacy
Form 990
Form PC
freedom
donations
financial statements
growing
success
---
categories:

fundraising
human rights
---
_html_body:

<p>2013 was a great year for Tor. The increasing awareness of the lack of privacy online, increasing Internet censorship around the world, and general interest in encryption has helped continue to keep us in the public mind. As a result, our supporters have increased our funding to keep us on the leading edge of our field, this of course, means you. We're happy to have more developers, advocates, and support volunteers. We're encouraged as the general public talks about Tor to their friends and neighbors. Join us as we continue to fight for your privacy and freedom on the Internet!<br />
After completing the standard audit, our 2013 state and federal <a href="https://www.torproject.org/about/financials.html.en" rel="nofollow">tax filings</a> are available. We publish all of our related tax documents because we believe in transparency. All US non-profit organizations are required by law to make their tax filings available to the public on request by US citizens. We want to make them available for all.<br />
Part of our transparency is simply publishing the tax documents for your review. The other part is publishing what we're <a href="https://trac.torproject.org/projects/tor/wiki/org/sponsors" rel="nofollow">working on</a> in detail. We hope you'll join us in furthering our mission (a) to develop, improve and distribute free, publicly available tools and programs that promote free speech, free expression, civic engagement and privacy rights online; (b) to conduct scientific research regarding, and to promote the use of and knowledge about, such tools, programs and related issues around the world; (c) to educate the general public around the world about privacy rights and anonymity issues connected to Internet use.<br />
All of this means you can look through our <a href="https://gitweb.torproject.org/" rel="nofollow">source code</a>, including our <a href="https://www.torproject.org/docs/documentation#DesignDoc" rel="nofollow">design documents</a>, and all open tasks, enhancements, and bugs available on our <a href="https://trac.torproject.org/" rel="nofollow">tracking system</a>. Our <a href="https://research.torproject.org/" rel="nofollow">research</a> reports are available as well. From a technical perspective, all of this free software, documentation, and code allows you and others to assess the safety and trustworthiness of our research and development. On another level, we have a 10 year track record of doing high quality work, saying what we're going to do, and doing what we said.<br />
Internet privacy and anonymity is more important and rare than ever. Please help keep us going through <a href="https://www.torproject.org/getinvolved/volunteer.html.en" rel="nofollow">getting involved</a>, <a href="https://www.torproject.org/donate/donate.html.en" rel="nofollow">donations</a>, or advocating for a free Internet with privacy, anonymity, and keeping control of your identity.</p>

---
_comments:

<a id="comment-66320"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66320" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 26, 2014</p>
    </div>
    <a href="#comment-66320">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66320" class="permalink" rel="bookmark">Thank you for taking the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for taking the time to write a blog post on this topic.  I hope others will join me in sustaining their financial and other forms of support to the project to dilute the amount of direct and indirect US government funding that the project currently receives.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66380"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66380" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 27, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66320" class="permalink" rel="bookmark">Thank you for taking the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66380">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66380" class="permalink" rel="bookmark">Thanks. It is alas still the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks. It is alas still the case that a lot of research money in the world comes from the governments. I'd love to figure out how to get foundations more involved here -- but historically, foundations shy away from technology and research.</p>
<p>I'd also love to start maintaining and growing a donor base on a similar model to EFF's. But we don't have the right people currently to get that going well. If that's you, please contact us!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66391"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66391" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 27, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-66391">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66391" class="permalink" rel="bookmark">&quot;I&#039;d love to figure out how</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"I'd love to figure out how to get foundations more involved here"</p>
<p>Foundations = corporations, no?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66410"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66410" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 27, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66391" class="permalink" rel="bookmark">&quot;I&#039;d love to figure out how</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66410">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66410" class="permalink" rel="bookmark">Well, yes but only in the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well, yes but only in the sense that The Tor Project is a corporation too.</p>
<p>I'm talking about Ford, MacArthur, Knight, Omidyar, etc. They each have a pile of money which they give to non-profits who further their mission. And we totally do further their mission, but sometimes it's in indirect and less obvious ways.</p>
<p>(Getting money from more traditional corporations is also possible, e.g. by doing audits of their Tor-related product, or adding features that they wish Tor would have, etc. But that's a different funder category.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66460"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66460" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 28, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-66460">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66460" class="permalink" rel="bookmark">Ford is a car making</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ford is a car making corporation</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66469"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66469" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 28, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66460" class="permalink" rel="bookmark">Ford is a car making</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66469">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66469" class="permalink" rel="bookmark">Roger was probably referring</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Roger was probably referring to <a href="http://www.fordfoundation.org/" rel="nofollow">http://www.fordfoundation.org/</a> which--according to wikipedia--has not had any financial relationship with the car making corporation for 40 years.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-66407"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66407" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 27, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-66407">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66407" class="permalink" rel="bookmark">You&#039;re absolutely right</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You're absolutely right about research funding and I don't think any reasonable person expects much to change on the research funding front, but my understanding of the 2013 documents is that US government funding (direct and indirectly provided) is not limited to research.That in itself is also not necessarily a bad thing, but all else being equal, we would probably agree that having more diverse "types" of funding (e.g. completely unrestricted, project-oriented grants, and research funding) and more diverse "sources" of funding (e.g. individuals from many countries, a variety of governments instead) might bolster user trust in the project and take some of the wind out of the sails of people spewing vitriolic accusations in pando articles.  </p>
<p>And I appreciate your comments on EFF's donor base, but think it's important to note that EFF is surprisingly opaque about its funding sources, and that's not limited to protecting the privacy of individual donors. For people paying attention to EFF's work, this has already and seriously compromised their neutrality and credibility when it comes to commenting on the policies of companies like Google, with whom EFF has multiple undisclosed financial and other conflicts of interest. </p>
<p>In terms of disclosure, your team deserves applause for being significantly *better* than EFF when it comes to being transparent in disclosing useful, substantive information in IRS 990 and other tax filings. People who pay attention might trust certain parts of EFF's work more if they were as transparent about disclosing potential financial conflicts of interest as your team has been!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-66321"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66321" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 26, 2014</p>
    </div>
    <a href="#comment-66321">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66321" class="permalink" rel="bookmark">I also wanted to add that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I also wanted to add that the updated 'Sponsors' page for 2014 is much more informative than previous iterations. Thank you for upholding the organization's commitment to transparency!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66379"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66379" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 27, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66321" class="permalink" rel="bookmark">I also wanted to add that</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66379">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66379" class="permalink" rel="bookmark">For those who wanted to look</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For those who wanted to look at that page, it's here:<br />
<a href="https://www.torproject.org/about/sponsors" rel="nofollow">https://www.torproject.org/about/sponsors</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66392"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66392" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 27, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-66392">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66392" class="permalink" rel="bookmark">I noticed that Google is one</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I noticed that Google is one of the active sponsors. Does it require Tor developers to <em>write bad code</em> so that the NSA or GCHQ could break into Tor's network? </p>
<p>Inserting backdoors is out of the question as the source code is open to the public for review. But writing bad code attracts less suspicion. Look at what the Heartbleed bug did to those who used OpenSSL.</p>
<p><strong>And why did Electronic Frontier Foundation stop sponsoring Tor?</strong> Is it because EFF has inside information that Tor is an agent of the NSA and cannot be counted on to advance the work of advocates of freedom of speech anonymously?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66411"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66411" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 27, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66392" class="permalink" rel="bookmark">I noticed that Google is one</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66411">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66411" class="permalink" rel="bookmark">No, Google&#039;s primary</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, Google's primary financial contribution is funding students to work with us on Google Summer of Code.<br />
<a href="https://blog.torproject.org/category/tags/gsoc" rel="nofollow">https://blog.torproject.org/category/tags/gsoc</a><br />
I'm happy to say that we have 13 such students this summer:<br />
<a href="https://trac.torproject.org/projects/tor/wiki/doc/gsoc" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/doc/gsoc</a></p>
<p>I say 'primary' because they also funded the design of Thandy long ago:<br />
<a href="http://google-opensource.blogspot.com/2009/03/thandy-secure-update-for-tor.html" rel="nofollow">http://google-opensource.blogspot.com/2009/03/thandy-secure-update-for-…</a><br />
<a href="http://theupdateframework.com/" rel="nofollow">http://theupdateframework.com/</a><br />
but now that we've switched Tor Browser away from being a bundle, we've moved away from thinking that Thandy is the right approach to secure update for us:<br />
<a href="https://trac.torproject.org/projects/tor/ticket/4234" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/4234</a></p>
<p>As for why EFF stopped funding Tor in 2005, they didn't have the money for it. They were hoping to raise more money to be able to fund us another year, but they didn't. As for why they don't fund us now... seems to me that EFF has a lot of great and important things they're working on too, yes? I am happy that at least so far we've been able to sustain ourselves without needing to go back to them. And in any case you'll notice that we still work with them on e.g. the EFF Tor Relay Challenge:<br />
<a href="https://www.eff.org/torchallenge/" rel="nofollow">https://www.eff.org/torchallenge/</a><br />
and on HTTPS Everywhere:<br />
<a href="https://www.eff.org/https-everywhere" rel="nofollow">https://www.eff.org/https-everywhere</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-66412"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66412" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 27, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66392" class="permalink" rel="bookmark">I noticed that Google is one</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66412">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66412" class="permalink" rel="bookmark">Regarding bad code and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Regarding bad code and Google sponsorsihp, if this is something that concerns you, please help the community review Tor dev's code to help make it better.</p>
<p>Regarding EFF, it seems like the sort of question you should ask the EFF.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-66472"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66472" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 28, 2014</p>
    </div>
    <a href="#comment-66472">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66472" class="permalink" rel="bookmark">Because the amount of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Because the amount of government donations in 2013 is substantial, this naturally  causes some distrust of their being a conflict of interest between "privacy" and donor's interests. This kind of problem needs to be in some way clarified for Tor users it appears.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66485"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66485" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 28, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66472" class="permalink" rel="bookmark">Because the amount of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66485">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66485" class="permalink" rel="bookmark">They&#039;re not donations;</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>They're not donations; they're contracts with specific things we do in exchange for the money. You can read about the specific things (generally, research and development tasks) at the 'sponsors' wiki page linked in the article. Then you can decide for yourself whether this is a good use of our time.</p>
<p>But yes, there is the general issue that the people who give us money influence what we spend our time on. That is true for the non-government funding as well. That's the point of publishing everything here and letting you learn what we're doing.</p>
<p>You might also like our 30c3 talk from December, where we have a slide and discussion on funding:<br />
<a href="http://media.ccc.de/browse/congress/2013/30C3_-_5423_-_en_-_saal_1_-_201312272030_-_the_tor_network_-_jacob_-_arma.html" rel="nofollow">http://media.ccc.de/browse/congress/2013/30C3_-_5423_-_en_-_saal_1_-_20…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66527"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66527" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 28, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-66527">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66527" class="permalink" rel="bookmark">If the Tor Project is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If the Tor Project is receiving money from a government under government contract (not a donation), then Tor Project is seemingly working for a government in some capacity and therefore a conflict of interests regarding "privacy" and cash flows may ensue now or in the future.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66532"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66532" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 28, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66527" class="permalink" rel="bookmark">If the Tor Project is</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66532">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66532" class="permalink" rel="bookmark">That&#039;s why we make sure to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That's why we make sure to only agree to do things we already want to do.</p>
<p>Also, that's why this whole blog post and thread is here, so you can watch and help.</p>
<p>For what it's worth, approximately no places just give you donations -- even the foundation money we've gotten has come in the form of milestones, deliverables, etc, and we get paid only once we complete them. Most people with money consider it bad stewardship of their money to just give it away -- they want to know whether you did the thing that you said you were going to do.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-66473"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66473" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 28, 2014</p>
    </div>
    <a href="#comment-66473">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66473" class="permalink" rel="bookmark">Large donations (usually) =</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Large donations (usually) = influence over policies</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-66538"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66538" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 28, 2014</p>
    </div>
    <a href="#comment-66538">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66538" class="permalink" rel="bookmark">To avoid any perceived</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>To avoid any perceived conflicts of interest, Tor Project could theoretically  refuse to do business with any governmental entity in the wold.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66615"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66615" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 29, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66538" class="permalink" rel="bookmark">To avoid any perceived</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66615">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66615" class="permalink" rel="bookmark">Yep!
But to avoid &#039;any</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yep!</p>
<p>But to avoid 'any perceived conflicts of interest', we'd best avoid corporate money too. And large private donors, based on the discussions of EFF funding and transparency above. Things start looking pretty grim pretty quickly, especially when you look at the budgets of some of our adversaries.</p>
<p>I'm much happier with our current approaches of "do everything in the open and show everybody what we're doing", plus "only take money for things we actually want to do". That also limits our growth, sure, but it ultimately puts us in a much more sustainable position.</p>
<p>We also talk about this balance issue in our 30c3 talk (linked in a comment above).</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-66618"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66618" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 29, 2014</p>
    </div>
    <a href="#comment-66618">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66618" class="permalink" rel="bookmark">Corporations are much </a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Corporations are much  different than governments (e.g., the N.S.A ) induces many more privacy worries because or their PRISM (Edward Snowden) than Ford Motor Company does in the average Tor user. Why not allow corporations to contribute and bar any governmental entities  from donating or contributing?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-68159"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-68159" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 09, 2014</p>
    </div>
    <a href="#comment-68159">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-68159" class="permalink" rel="bookmark">&quot;They&#039;re not donations;</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"They're not donations; they're contracts with specific things we do in exchange for the money. You can read about the specific things (generally, research and development tasks) at the 'sponsors' wiki page linked in the article."  The details need to be posted on Torproject site, not the sponsor. I have asked before, and received no reply, "what specific changes to Firefox has the US government demanded in exchange for its dollars?"</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-68408"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-68408" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 10, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-68159" class="permalink" rel="bookmark">&quot;They&#039;re not donations;</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-68408">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-68408" class="permalink" rel="bookmark">I think you missed the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think you missed the sponsor wiki page, as linked in the article:<br />
<a href="https://trac.torproject.org/projects/tor/wiki/org/sponsors" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/org/sponsors</a><br />
You should click on each of those if you want to learn details.</p>
<p>As for what we're changing in Firefox, see<br />
<a href="https://www.torproject.org/projects/torbrowser/design/" rel="nofollow">https://www.torproject.org/projects/torbrowser/design/</a><br />
for the general goals, and then see<br />
<a href="https://trac.torproject.org/projects/tor/wiki/org/sponsors/SponsorP" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/org/sponsors/SponsorP</a><br />
for specific work that we're doing. ("Demanded" isn't really the right word though -- we proposed a set of things we want to do and a price, and they said yes.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-68448"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-68448" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 10, 2014</p>
    </div>
    <a href="#comment-68448">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-68448" class="permalink" rel="bookmark">Any money coming from a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Any money coming from a governmental entity for "contracted" work still causes privacy problems with Tor users.  Cash flow= a subtle influence on potential future developments. This work appears to have been contracted about the time of Edward Snowden's revelations. Maybe the government wants to gain some leverage in the workings of Tor Project by using financial contracts to leverage increased influence over time.<br />
 To end the obfuscation of  the situation, perhaps TorProject would be practical in doing "contracted" work for corporations like Ford Motors, Caterpillar, Disney,etc. and not any governmental entity on earth.If TorProject  stops dealing with any governmental entity anywhere in the world  contractually  in the future, privacy will be restored to a greater degree and Trust in Tor may cause other donors (not governments) to donate or contract with TorProject.</p>
</div>
  </div>
</article>
<!-- Comment END -->
