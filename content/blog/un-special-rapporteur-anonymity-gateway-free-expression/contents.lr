title: UN Special Rapporteur: Anonymity Is Gateway to Free Expression
---
pub_date: 2015-05-29
---
author: wseltzer
---
tags:

free speech
anonymity
law
human rights
---
categories: human rights
---
_html_body:

<p>We at the Tor Project have <a href="https://www.torproject.org/about/torusers.html.en" rel="nofollow">long</a> <a href="https://blog.torproject.org/blog/anonymous-publishing-and-risking-execution" rel="nofollow">said</a> that Tor is a technology for free expression. Today, that view was endorsed by UN Special Rapporteur David Kaye in <a href="http://www.ohchr.org/EN/ISSUES/FREEDOMOPINION/Pages/OpinionIndex.aspx" rel="nofollow">a new report on encryption and anonymity</a>. The report, a close look at international law and its relation to technology, concludes that encryption and anonymity technologies are essential to the protection of human rights to privacy and freedom of expression and opinion:</p>

<blockquote><p>Encryption and anonymity, separately or together, create a zone of privacy to protect opinion and belief. For instance, they enable private communications and can shield an opinion from outside scrutiny, particularly important in hostile political, social, religious and legal environments. Where States impose unlawful censorship through filtering and other technologies, the use of encryption and anonymity may empower individuals to circumvent barriers and access information and ideas without the intrusion of authorities. Journalists, researchers, lawyers and civil society rely on encryption and anonymity to shield themselves (and their sources, clients and partners) from surveillance and harassment. The ability to search the web, develop ideas and communicate securely may be the only way in which many can explore basic aspects of identity, such as one’s gender, religion, ethnicity, national origin or sexuality. Artists rely on encryption and anonymity to safeguard and protect their right to expression, especially in situations where it is not only the State creating limitations but also society that does not tolerate unconventional opinions or expression.</p></blockquote>

<p>The report points to the Tor network specifically, noting that anonymity is critical to protect privacy against identification through metadata analysis. "A common human desire to protect one’s identity from the crowd, anonymity may liberate a user to explore and impart ideas and opinions more than she would using her actual identity." In the protection of free expression, anonymity technology is thus a necessary counterpart to encryption, giving the individual the ability to choose both what to say and to whom to reveal that she is saying it. </p>

<p>The Kaye Report recognizes that technologies can be used for harm as well as for good, but that does not mean they may be banned. Rather, human rights law offers a strict framework for evaluation of government-imposed limits: "Restrictions on encryption and anonymity, as enablers of the right to freedom of expression... must be provided for by law; may only be imposed for legitimate grounds; and must conform to the strict tests of necessity and proportionality." That means that legal restrictions must be publicly and transparently legislated, with judicial safeguards on their application; they must be applied narrowly; and they must be proportional to the objectives of the law. "Because anonymity facilitates opinion and expression in significant ways online, States should protect it and generally not restrict the technologies that provide it."</p>

<p>The Tor Project is pleased to have contributed to the report, and we heartily endorse its conclusion:</p>

<blockquote><p>The use of encryption and anonymity tools and better digital literacy should be encouraged. The Special Rapporteur, recognizing that the value of encryption and anonymity tools depends on their widespread adoption, encourages States, civil society organizations and corporations to engage in a campaign to bring encryption by design and default to users around the world and, where necessary, to ensure that users at risk be provided the tools to exercise their right to freedom of opinion and expression securely.</p></blockquote>

---
_comments:

<a id="comment-94166"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94166" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 28, 2015</p>
    </div>
    <a href="#comment-94166">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94166" class="permalink" rel="bookmark">No one in the world has</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No one in the world has 'cracked' strong properly implemented encryption.<br />
40 and 56 bit encryption can be cracked very easily. However, 256 bit AES encryption is still uncrackable and it probably will be for several decades.<br />
I'm glad that that <a href="https://www.torproject.org/" rel="nofollow">https://www.torproject.org/</a> uses 256 bit AES encryption and has no weaknesses in it's encryption.<br />
Well done, Tor Project, for keeping Tor and your website secure.<br />
Everyone should use Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-94368"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94368" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 04, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-94166" class="permalink" rel="bookmark">No one in the world has</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-94368">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94368" class="permalink" rel="bookmark">AES 256-bit is extremely</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>AES 256-bit is extremely difficult to break, but it's not impossible, we have to be careful about it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94738"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94738" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 09, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-94166" class="permalink" rel="bookmark">No one in the world has</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-94738">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94738" class="permalink" rel="bookmark">True, but as technology</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>True, but as technology advances.....Titanic.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-94174"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94174" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 29, 2015</p>
    </div>
    <a href="#comment-94174">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94174" class="permalink" rel="bookmark">Yes Yes</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes Yes</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94191"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94191" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 30, 2015</p>
    </div>
    <a href="#comment-94191">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94191" class="permalink" rel="bookmark">&gt;A common human desire to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt;A common human desire to protect one’s identity from the crowd, anonymity may liberate a user to explore and impart ideas and opinions more than she would using her actual identity.<br />
I know that feel</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94364"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94364" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 04, 2015</p>
    </div>
    <a href="#comment-94364">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94364" class="permalink" rel="bookmark">&quot;I&#039;m glad that that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"I'm glad that that <a href="https://www.torproject.org/" rel="nofollow">https://www.torproject.org/</a> uses 256 bit AES encryption and has no weaknesses..."</p>
<p>The homepage from TBBs browser source<br />
https://*.mozilla.org<br />
has really WORST encryption.<br />
Test it with  <a href="https://www.ssllabs.com/ssltest/" rel="nofollow">https://www.ssllabs.com/ssltest/</a> .</p>
<p>Why?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94365"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94365" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 04, 2015</p>
    </div>
    <a href="#comment-94365">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94365" class="permalink" rel="bookmark">Consider the security of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Consider the security of updating TBB, inside updater, or download full TBB package, import public signing key file then verify the package manually, which is better?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-94490"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94490" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">June 07, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-94365" class="permalink" rel="bookmark">Consider the security of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-94490">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94490" class="permalink" rel="bookmark">This is an excellent</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is an excellent question for you to ask on <a href="https://tor.stackexchange.com/" rel="nofollow">https://tor.stackexchange.com/</a> (rather than on an unrelated blog post). Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-94585"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94585" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 08, 2015</p>
    </div>
    <a href="#comment-94585">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94585" class="permalink" rel="bookmark">Its way past time that an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Its way past time that an organisation such as the UN endorsed Tor. I see it as one of the best things we have got to protect our freedom of expression and right to information.</p>
<p>Having said that, you have to hand it to the UN for siding with these basic rights. Its noble that they are weighing in on an ongoing debate. The debate about encryption and anonymity.</p>
<p>This ongoing debate has seemed completely the opposite as government after government has surfaced with an anti-privacy and anti-anonymity line of reasoning with plenty of rhetoric to scare us half to death about these things. Users are being made to feel like criminals.</p>
<p>In this climate of fear and jack-booted insistence on thinking one way only, the Tor project and this endorsement by the UN is a small beacon of hope.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-95659"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-95659" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 20, 2015</p>
    </div>
    <a href="#comment-95659">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-95659" class="permalink" rel="bookmark">Until yesterday, I was</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Until yesterday, I was strongly behind the mindset which sought to secure and anonymize personal communications. Tor seemed like a great idea.<br />
But yesterday, my wife's computer was hit by the CTB-Locker malware which leverages that same anonymity to utterly destroy years of our private lives.<br />
Videos of our children growing up, letters and articles researched and written, music painstakingly collected and organized, photos of loved ones taken over decades...<br />
The security afforded the criminal who has put us in this position is provided by "onion" and Tor functionality, regardless of our ineptitude in failing to protect ourselves from the attack.<br />
I have reversed course on my fondness for the urge to keep Big Brother from learning things about the electronic activities of people.<br />
These Tor and Onion things are the equivalent of guns and knives, allowing those with strength and no ethic to forcefully deprive the common person of their most precious posessions.<br />
I would much rather have had my car stolen, my house windows broken, or my living room vandalized. I could painted the walls or got around on the bus.<br />
Now, because the ransomware has damaged our NAS backups, there's practically no hope.<br />
At least, when it comes to gun control, people can defend themselves or lean on authorities to investigate and find out who did the shooting.<br />
With information anonymity the likes of which you provide to anyone, there's no one who can help.<br />
Thanks for nothing. In my mind, it is perfectly clear that worked very hard to help criminals rob me of a large part of my family's most precious belongings.<br />
I feel utterly violated.<br />
The obligation to protect people from maliciousness should come with the privilege of being able to provide the tools allowing them to perpetrate. Cloaking communications from the community which seeks to extend protection to those not strong enough to protect themselves amounts to either an abdication of this responsibility or else a partnership with those acting so despicably.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-95790"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-95790" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 23, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-95659" class="permalink" rel="bookmark">Until yesterday, I was</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-95790">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-95790" class="permalink" rel="bookmark">Pay to get the decryption</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Pay to get the decryption key and make offline backups so this doesn't happen to you again. The world should not have to live without political freedom because you blame online privacy for this.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-98298"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-98298" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 25, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-95659" class="permalink" rel="bookmark">Until yesterday, I was</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-98298">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-98298" class="permalink" rel="bookmark">I believe you are barking up</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I believe you are barking up the wrong tree. How would you react if your children fell victim in a high school massacre. How would guns have defended them? I believe you'd be just barking up the other tree in that case. </p>
<p>It is not the tools that are a problem, without a knife you cannot slice up the pig you want to eat, but also your children may be killed by a maniac using a knife. </p>
<p>For me there is no legitimate use of guns, still they continue being sold. There is a legitimate use of knifes, outweighing the problems their misuse can cause. </p>
<p>For Tor, it outweighs by far its misuse. Tor saves lives!</p>
<p>In your case, the problem seems to reside with you, not taking appropriate backups. Accept that some things have to be learned the hard way.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
