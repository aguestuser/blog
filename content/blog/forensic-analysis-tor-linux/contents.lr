title: Forensic Analysis of Tor on Linux
---
pub_date: 2013-04-13
---
author: Runa
---
tags:

tor
tbb
forensic analysis
linux
---
categories:

applications
network
---
_html_body:

<p>As part of a deliverable for two of our sponsors (<a href="https://trac.torproject.org/projects/tor/wiki/org/sponsors/SponsorJ" rel="nofollow">Sponsor J, </a><a href="https://trac.torproject.org/projects/tor/wiki/org/sponsors/SponsorL" rel="nofollow">Sponsor L</a>), I have been working on a forensic analysis of the Tor Browser Bundle. In this three part series, I will summarize the most interesting or significant traces left behind after using the bundle. This post will cover Debian Linux (<a href="https://trac.torproject.org/projects/tor/ticket/8166" rel="nofollow">#8166</a>), part two will cover Windows 7, and part three will cover OS X 10.8.</p>

<h3>Process</h3>

<p>I set up a virtual machine with a fresh install of Debian 6.0 Squeeze, logged in once and shut it down cleanly. I then connected the virtual drive to another virtual machine and used <em>dd</em> to create an image of the drive. I also used <em>hashdeep</em> to compute hashes for every file on the drive, and <em>rsync</em> to copy all the files over to an external drive.</p>

<p>After having secured a copy of the clean virtual machine, I rebooted the system, connected an external drive, and copied the Tor Browser Bundle (version 2.3.25-6, 64-bit) from the external drive to my Debian home directory. I extracted the package archive and started the Tor Browser Bundle by running ./start-tor-browser inside the Tor Browser directory.</p>

<p>Once the Tor Browser was up and running, I browsed to a few pages, read a few paragraphs here and there, clicked on a few links, and then shut it down by closing the Tor Browser and clicking on the <em>Exit</em>-button in Vidalia. The Tor Browser did not crash and I did not see any error messages. I deleted the Tor Browser directory and the tarball using <em>rm -rf</em>.</p>

<p>I repeated the steps with <em>dd</em>, <em>hashdeep</em>, and <em>rsync</em> to create a copy of the tainted virtual machine.</p>

<h3>Results</h3>

<p>Using <em>hashdeep</em>, I compared the hashes from the tainted virtual machine against the hashes from the clean virtual machine: <a href="https://trac.torproject.org/projects/tor/attachment/ticket/8166/debian_changed_files.txt" rel="nofollow">68 files</a> had a hash that did not match any of the hashes in the clean set. The most interesting files are:</p>

<p><strong>~/.local/share/gvfs-metadata/home</strong>: contains the filename of the Tor Browser Bundle tarball: <em>tor-browser-gnu-linux-x86_64-2.3.25-5-dev-en-US.tar.gz</em>. GVFS is the virtual filesystem for the GNOME desktop, so this result will probably vary depending on the window manager used. I have created <a href="https://trac.torproject.org/projects/tor/ticket/8695" rel="nofollow">#8695</a> for this issue.</p>

<p><strong>~/.xsession-errors</strong>: contains the following string: <em>“Window manager warning: Buggy client sent a _NET_ACTIVE_WINDOW message with a timestamp of 0 for 0x3800089 (Tor Browse)”</em>. It is worth noting that a file named <em>.xsession-errors.old</em> could also exist. I have created <a href="https://trac.torproject.org/projects/tor/ticket/8696" rel="nofollow">#8696</a> for this issue.</p>

<p><strong>~/.bash_history</strong>: contains a record of commands typed into the terminal. I started the Tor Browser Bundle from the command line, so this file contains lines such as <em>./start-tor-browser</em>. I have created <a href="https://trac.torproject.org/projects/tor/ticket/8697" rel="nofollow">#8697</a> for this issue.</p>

<p><strong>/var/log/daemon.log</strong>, <strong>/var/log/syslog</strong>, <strong>/var/log/kern.log</strong>, <strong>/var/log/messages</strong>: contains information about attached devices. I had an external drive attached to the virtual machine, so these files contain lines such as <em>“Mounted /dev/sdb1 (Read-Write, label “THA”, NTFS 3.1)”</em> and <em>“Initializing USB Mass Storage driver…”</em>.</p>

---
_comments:

<a id="comment-19594"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19594" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 12, 2013</p>
    </div>
    <a href="#comment-19594">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19594" class="permalink" rel="bookmark">regarding</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>regarding ~/.bash_history:</p>
<p>running<br />
&gt; unset $HISTFILE</p>
<p>in the terminal used to run the bundle prevents logging (bash on Ubuntu; ymmv)</p>
<p>Terminal commands are logged to $HISTFILE when the terminal exits, so this command can be run at any time and will prevent logging in the current terminal.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19603"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19603" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 13, 2013</p>
    </div>
    <a href="#comment-19603">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19603" class="permalink" rel="bookmark">what about a for browser VM?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>what about a for browser VM?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-24691"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-24691" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 16, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19603" class="permalink" rel="bookmark">what about a for browser VM?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-24691">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-24691" class="permalink" rel="bookmark">Whonix?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Whonix?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-19604"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19604" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 13, 2013</p>
    </div>
    <a href="#comment-19604">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19604" class="permalink" rel="bookmark">You only looked at files.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You only looked at files. There are probably a lot of traces left around that aren't in files at all: contents of deleted temporary files, stuff in the swap, etc. Some of it may show not only that you've run the TBB, but something significant about what you're doing. I don't think the TBB can really do anything to make a system forensics proof against somebody who has physical possession of the machine. And even if you clean a VM, that may or may not clean the physical machine it was on.</p>
<p>You can look for some of this by dding out entire disks, or perhaps by comparing the virtual disk files directly. But it's going to be a lot of work to figure out which changes are TBB traces and which aren't. And after you figure it out, you may very well not be able to do anything about it.</p>
<p>There may also be unusual or error conditions that cause trouble. For example, it may be possible to (intentionally or unintentionally) crash the browser without having it clean up various temporary data, or to get it to forget to clean something, or to get it to drop some piece of debugging information somewhere.</p>
<p>You may be trying to do something that's simply not achievable. If you want to be truly forensics-proof, I think you're probably at least going to have to boot from the Tor media. And there's a good chance you're going to have to boot the PHYSICAL MACHINE from the Tor media.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-19610"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19610" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 13, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19604" class="permalink" rel="bookmark">You only looked at files.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19610">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19610" class="permalink" rel="bookmark">Sounds like good reasons to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sounds like good reasons to use Tails or Linux Liberte` if not leaving traces is necessary.</p>
<p>Of course, you have to trust a respective third-party product. In the case of Tails, the ambiguity of the affiliation with the Tor Project doesn't help instill confidence. (See<br />
<a href="https://blog.torproject.org/blog/new-tor-browser-bundles-firefox-1705esr#comment-19547" rel="nofollow">https://blog.torproject.org/blog/new-tor-browser-bundles-firefox-1705es…</a> )</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-19635"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19635" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 15, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19610" class="permalink" rel="bookmark">Sounds like good reasons to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19635">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19635" class="permalink" rel="bookmark">If you download Tails or</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you download Tails or Liberte Linux you leave traces that you downloaded Tails or Liberte Linux. These are equally difficult to remove.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19713"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19713" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 18, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19610" class="permalink" rel="bookmark">Sounds like good reasons to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19713">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19713" class="permalink" rel="bookmark">So now it&#039;s this. What sort</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So now it's this. What sort of public announcement would make you more confident?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-19605"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19605" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 13, 2013</p>
    </div>
    <a href="#comment-19605">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19605" class="permalink" rel="bookmark">Could you do a part 4 on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Could you do a part 4 on FreeBSD?  I'd be especially curious to see how FreeBSD compares to Linux.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19623"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19623" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 14, 2013</p>
    </div>
    <a href="#comment-19623">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19623" class="permalink" rel="bookmark">Too little, too late, but</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Too little, too late, but thank you very much for doing this, Runa!</p>
<p>I am unable to contact Tails, and the Tails forum is down, but there is also significant leakage about usage of this supposedly "amnesiac" OS booted from a R/O DVD.</p>
<p>In a desktop or laptop running Debian Squeeze (with Gnome desktop), try</p>
<p>      find -mtime -1 | grep metadata | grep gconf</p>
<p>Nautilus stores information about the desktop icon you see when you mount an encrypted USB or DVD.  This information includes the name, time, and position of the icon.</p>
<p>Please thank Tom L for his work, especially on tutorials for the masses.</p>
<p>If this comment works I may add more later about metadata issues when running TBB.  Runa, I think you need to do more faux "work" than you describe above to see how bad the problem is.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-19633"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19633" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  Runa
  </article>
    <div class="comment-header">
      <p class="comment__submitted">Runa said:</p>
      <p class="date-time">April 15, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19623" class="permalink" rel="bookmark">Too little, too late, but</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19633">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19633" class="permalink" rel="bookmark">Please create tickets on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please create tickets on <a href="https://bugs.torproject.org/" rel="nofollow">https://bugs.torproject.org/</a> for metadata issues when running TBB, I'd love to know about them.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-19630"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19630" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 15, 2013</p>
    </div>
    <a href="#comment-19630">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19630" class="permalink" rel="bookmark">Using a Live Linux Distro</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Using a Live Linux Distro with encrypted AES persistence solve the problem of leakage information.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-19636"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19636" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 15, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19630" class="permalink" rel="bookmark">Using a Live Linux Distro</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19636">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19636" class="permalink" rel="bookmark">How do you download it</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How do you download it without leaving traces? It's a bootstrap problem. Download without Tor? Download with your every day OS? How to delete traces of download of your every day OS?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-19950"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19950" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 24, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19636" class="permalink" rel="bookmark">How do you download it</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19950">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19950" class="permalink" rel="bookmark">Does not exist total</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does not exist total security but you can try to limit the inconvenience....<br />
- Use VPN with no logs<br />
- Use a live OS distro on Usb stick to keep separate from the rest of the computer (I use Linux on a 16GB micro SDHC)<br />
- Use encryption for whole disk or partition or files, where store personal data, leakage trace left by the OS on your local system<br />
In my case:<br />
1° passphrase to unlock encrypted AES partition to run OS with my personal settings (in case of incorrect key it starts a normal linux live distro)<br />
2° passphrase to unlock encrypted AES container to run Tor and my web hidden server and gain access personal data<br />
3° passphrase to unlock an hidden encryped AES file inside the previous container where i preserve key file, bank details, password, private photos<br />
AES is better than nothing<br />
- Use Tor and hijack all outgoing Internet traffic<br />
- Use Advanced Tor options (control port 9151)<br />
- Swap is disabled<br />
- Change MacAddress<br />
- Don't use Tor to download big size files or you'd be old before the end<br />
- Use strong and long Passphrase (mine are about 29 characters)<br />
- Use Bitcoin<br />
- Use PGP<br />
- Have many identities<br />
- I forgot something ?<br />
- Good Look</p>
<p>Anything can be hidden in the dark ... you just hope not to meet ever!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-19646"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19646" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 15, 2013</p>
    </div>
    <a href="#comment-19646">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19646" class="permalink" rel="bookmark">Runa, 
I&#039;ll try, but in case</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Runa, </p>
<p>I'll try, but in case it doesn't work: one quick point: in doing similar studies of TBB to what you did, I observed that when you start with a fresh TBB under Debian stable, as you surf to dozens of popular websites, more DOM storage files are created, and they seem to change in ways I cannot yet predict. ( I hash, do one browser action, rehash, and the results appear inconsistent.)   I speculate that this might be due in part to "harmless" attempts to update indexes for the various tables, but fear this could be used to harm users in certain circumstances.</p>
<p>Also as noted above, nautilus, gedit, and other hard to avoid utilities add cruft such as plaintext names of encrypted files, paths, and... at least one my Debian stable, the situation is not good at all.</p>
<p>@ preceding comment, regarding "using a live Linux distro with encrypted AES persistence solves the problem".  </p>
<p>I disagree, and urge you to read about concerns about AES (Bruce Schneier has explained better than I can) and about wear leveling (if you boot Tails from a USB).  Beyond that, many Tor users feel the threat model badly needs revision.  </p>
<p>I've looked a bit at Tails too and I think Tails also has potential metadata issues.  A simple example: when a Debian stable user simply sticks a Tails DVD in a CD tray, or a LUKS encrypted USB in a USB tray, Nautilus seems to record the name and a large integer seems to be a timestamp.  If so, even a remote intruder who simply has the permissions of the ordinary user can probably deduce quite a bit about what the Debian user was doing when.  (If you want to boot from a Tails DVD, it can be inconvenient to try to get the DVD in the tray unless you do this while your Debian OS is booted.)</p>
<p>We know that some of our adversaries know this, have been exploiting such issues with thousands of trained humans, and further, are having those humans train an AI system which will do it better, automatically, and potentially on a much more massive scale in the next few years.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19693"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19693" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 17, 2013</p>
    </div>
    <a href="#comment-19693">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19693" class="permalink" rel="bookmark">I use Knoppix, not tails. I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I use Knoppix, not tails. I load the OS from USB stick. As a live distro it gives me the opportunity to create an encrypted persistence where to save my data, customization, applications, browser history, bookmarks, TBB, and so on.<br />
Intruders who were in possession of the USB could not access the data encrypted (I hope) stored in the file.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19696"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19696" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 17, 2013</p>
    </div>
    <a href="#comment-19696">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19696" class="permalink" rel="bookmark">Runa: Which objective are</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Runa: Which objective are you going after?</p>
<ol>
<li>to counter forensic analysis on a machine that the user does not own, like in a library or internetcafe?</li>
<li>to counter forensic analysis on a machine that the user owns and has admin rights to?</li>
</ol>
<p>These objectives are different, and from this post it appears that it's option number one.</p>
<p>For option number two, whole disc encryption seems to be the fastest means to this end. But this solution may not be available for the first objective, since you need admin rights to install the disc encryption software.<br />
See: <a href="https://www.schneier.com/essay-199.html" rel="nofollow">https://www.schneier.com/essay-199.html</a><br />
Also, OpenBSD supports encrypted partitions from scratch.</p>
<p>Another thought:<br />
USB sticks leave their serial number in the logs in Linux, which you may already have noticed. Windows leaves serial numbers in the registry and/or home directory.</p>
<p>A third point:<br />
Admins of some public computers have disabled USB ports or DVD's from reading and booting their system, which is another cause for concern.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19699"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19699" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 17, 2013</p>
    </div>
    <a href="#comment-19699">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19699" class="permalink" rel="bookmark">I&#039;m the first &quot;Anonymous&quot;,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm the first "Anonymous", with the "You only looked at files" comment. The other anonymous comments aren't mine.</p>
<p>Last time I looked, Knoppix would by default search the disk for any swap partition and happily start swapping on it. I don't know if they encrypt the swap or not. If it's not encrypted, you're boned. If it is encrypted, the presence of the encrypted swap data from Knoppix on the probably unencrypted swap partition of your computer is going to at least raise questions about when you booted Knoppix and why. If you forget to type the advanced "no swap" boot option, you lose.</p>
<p>Tails at least doesn't do that. But there's still the rubber hose attack if they find the USB stick. And as the other anonymous points out, if you ever put that stick into your computer while your regular OS is running, you're made as a user.</p>
<p>I still think it's a total fool's errand for a pure user mode suite like the TBB to try to hide the mere fact of use, or probably even the browsing history, from serious forensic analysis. Casual poking around by your average roommate, yes. Intelligence agencies, or national-level law enforcement when  they're motivated? No. And the day they decide automate it, every beat cop in Iran will be able to tell.</p>
<p>Far safer to just tell users who need to be worried about forensics to "turn off the computer, go retrieve this USB key, boot from it, do secret stuff, then turn off the computer again, take a walk and hide the key, come back and boot the computer again".</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19785"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19785" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 19, 2013</p>
    </div>
    <a href="#comment-19785">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19785" class="permalink" rel="bookmark">Good job, keep it up. Even</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good job, keep it up. Even if the tor developers can't change it, the information is appreciated.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19795"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19795" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 20, 2013</p>
    </div>
    <a href="#comment-19795">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19795" class="permalink" rel="bookmark">#Sounds like good reasons to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>#Sounds like good reasons to use Tails#</p>
<p>I dont know.........<br />
Tails developer insistent ignore problem of NON-persistent EntryGuard.<br />
Implemented like  Bridge mode  in TAILS. The average TAILS user don't hack his copy of TAILS.</p>
<p>They work this,say that but<br />
dont solve this very hard problem.<br />
Why?  May team themis?<br />
I would say this is a serious problem.</p>
<p><a href="https://www.torproject.org/docs/faq.html.en#EntryGuards" rel="nofollow">https://www.torproject.org/docs/faq.html.en#EntryGuards</a><br />
<a href="https://www.torproject.org/docs/tor-manual.html.en" rel="nofollow">https://www.torproject.org/docs/tor-manual.html.en</a><br />
 UseEntryGuards 0|1<br />
This is desirable because constantly changing servers increases the odds that an adversary who owns some servers will observe a fraction of your paths.<br />
<a href="https://gitweb.torproject.org/tor.git/blob/tor-0.2.4.12-alpha:/ChangeLog" rel="nofollow">https://gitweb.torproject.org/tor.git/blob/tor-0.2.4.12-alpha:/ChangeLog</a><br />
 Raise the default time that a client keeps an entry guard from<br />
      "1-2 months" to "2-3 months", as suggested by Tariq Elahi's WPES<br />
      2012 paper.</p>
<p>Think about this.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19832"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19832" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 21, 2013</p>
    </div>
    <a href="#comment-19832">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19832" class="permalink" rel="bookmark">&quot;But there&#039;s still the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"But there's still the rubber hose attack if they find the USB stick. And as the other anonymous points out, if you ever put that stick into your computer while your regular OS is running, you're made as a user."</p>
<p>Yes, and the fact that US intelligence attempts to keep a database of the unique identifier of every USB stick (and hard drive) in the world is public information.  (They say they use this for forensic analysis of "pocket litter" when they search the house of someone suspected of opposing US policies.)</p>
<p>I believe the previous comment may refer to the Tails forum nixing a detailed analysis of confirmation attacks.  In any case, I also urge you (Runa) to liase with the Tails developers in performing further forensic analysis of TBB running under Debian stable and other popular OSs, and of Tails.  I also agree with the poster who suggested more carefully defining what kind of potential problems you seek to prevent.</p>
<p>I decided my own results are too inconsistent to try to file a bug report; rather, it seems better for the Tor and Tails developers to define their goals and perform their own analysis using hints above about some things to look for.</p>
<p>Thanks for your work on this, and more please!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19928"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19928" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 24, 2013</p>
    </div>
    <a href="#comment-19928">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19928" class="permalink" rel="bookmark">Can somebody make a USB plug</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can somebody make a USB plug that sits between a USB device and the computer that changes these unique IDs?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-21201"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-21201" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 15, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19928" class="permalink" rel="bookmark">Can somebody make a USB plug</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-21201">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-21201" class="permalink" rel="bookmark">thats a very good idea</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thats a very good idea</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
