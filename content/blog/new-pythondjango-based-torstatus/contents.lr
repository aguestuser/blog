title: New Python/Django-based TorStatus
---
pub_date: 2011-08-29
---
author: karsten
---
tags:

tor status
tor network
relay information
python
django
wesleyan hfoss
get involved
---
categories:

network
relays
---
_html_body:

<p>We're excited to announce that a group of students from Wesleyan has created a new <a href="http://www.torstatusbeta.org/" rel="nofollow">TorStatus</a> website written in Python.  Their new website is a fine addition to Kasimir Gabert's <a href="http://torstatus.blutmagie.de/" rel="nofollow">Tor Network Status</a> page.  Kasimir's page is currently the most popular website to learn about running Tor relays.</p>

<p>We're glad that we now have a new Python/Django-based TorStatus, as many developers in the Tor community have been working on complementary Tor Python libraries.</p>

<p>The new TorStatus started out as a rewrite of the old PHP-based TorStatus, so we're building from Kasimir's great ideas.  The new TorStatus adds a new landing page, paged results that decrease latency, and carefully crafted graphs with a focus on aesthetics.  But it's not only the features that make this code so great.  We believe that many more developers will be interested in modifying and extending the codebase!  At least that's much simpler now with the move to Python/Django.</p>

<p>The new TorStatus project was done by <a href="http://ndanner.web.wesleyan.edu/" rel="nofollow">Norman Danner</a>'s students during their summer <a href="http://hfoss.wesleyan.edu/summer-institute-2011" rel="nofollow">The Humanitarian Free and Open Source Software at Wesleyan University</a> course.  Thanks to Jeremy, Diego, and Vlad for designing, coding, and testing this new TorStatus!  Thanks to Damian for co-mentoring the students on Tor's side.</p>

<p>What's next?  Want to help out with coding on the new TorStatus?  You should start by looking at the <a href="https://gitweb.torproject.org/torstatus.git" rel="nofollow">codebase</a> and the list of open <a href="https://trac.torproject.org/projects/tor/query?component=TorStatus&amp;status=!closed" rel="nofollow">tickets</a>.  Feel free to leave a comment here or drop by <a href="irc://irc.oftc.net/tor" rel="nofollow">#tor-dev</a> if you have suggestions or are interested in helping out!</p>

---
_comments:

<a id="comment-11271"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11271" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 29, 2011</p>
    </div>
    <a href="#comment-11271">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11271" class="permalink" rel="bookmark">Could you guys quit using</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Could you guys quit using KB/s and start using real<br />
bandwidth units that ISP people recognize? That is<br />
bits/sec in powers of ten. NOT 'K' which is ambiguous<br />
and not even a real prefix. And 'bits', not bytes. We're<br />
sorry if Linux adopted the incorrect unit specs. But<br />
the rest of the real network world uses 'bits/sec'...<br />
cisco/juniper, tier-n's, your bandwidth provider etc.<br />
Tor is a bandwith application, NOT a disk storage<br />
application. It should use 'bits/sec' in all areas.<br />
See also: <a href="http://en.wikipedia.org/wiki/Binary_prefix" rel="nofollow">http://en.wikipedia.org/wiki/Binary_prefix</a><br />
Decimal SI prefixes. Thank you!!! (those of us sick<br />
of converting and dealing with ambiguous units :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-11279"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11279" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 30, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-11271" class="permalink" rel="bookmark">Could you guys quit using</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-11279">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11279" class="permalink" rel="bookmark">As a former network</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As a former network engineer, this has been a gripe of mine forever. However, the consensus feels users understand kilobytes per second better than bits per second. Even though every bit of networking gear and bandwidth you buy is in bits per second.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-11290"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11290" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 31, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-11290">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11290" class="permalink" rel="bookmark">Sure, they may understand it</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sure, they may understand it better from burning DVD's full of whatever with their storage app. If they're lucky... because not even Microsoft demonstrates correct usage. So they learn bad definitions in that space.</p>
<p>Then they break down when they try interfacing their purely network app (Tor) with their network provider (who happens to properly quote bits/sec). And the user is left trying to convert units and usually failing. Often because...</p>
<p>'K' is not defined in IEC or SI. It has no standard meaning. So Tor's 'K'B/sec could be binary or decimal. It's not documented. And 'B' looks like binary. So who's to know. Until they get their bill. And the words in ConstrainedSockSize present a conflicting view of 'K'B as well. Unless you're actually a coder who might recognize the implied meaning in light of system calls.</p>
<p>This isn't a voice to the Net Eng choir. It's a voice against continuing to choose to perpetuate the .consensus.look.feel.common.ambiguity.user. ways in the face of now well defined and long ratified international standards meant to finally fix those problems. Why be one of those non-standard apps?</p>
<p>I'd rather have a FAQ entry that tries to educate me than one that tries to placate me.</p>
<p>Tor's users aren't going to revolt from changing the display labels from KB to kbits or kB or even [K|M|..]iB, all where proper. Because the only place they are used is relay and exit ops (a very small subset of users at that). That subset will probably thank you for the sanity and making their interfacing, graphing, etc... with the network world, easier.</p>
<p>(Yes, I recognize AccountingMax as a good and proper feature to have too. Just not it's use of 'K' :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-11272"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11272" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 29, 2011</p>
    </div>
    <a href="#comment-11272">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11272" class="permalink" rel="bookmark">There are multiple</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There are multiple selections of the same title in the display options add/remove up/down page thing. Very confusing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-11273"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11273" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 29, 2011</p>
    </div>
    <a href="#comment-11273">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11273" class="permalink" rel="bookmark">Relays per page needs bumped</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Relays per page needs bumped to allow 'all', not just 200.<br />
So that an export of the entire selected set under view can occur.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-11274"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11274" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 29, 2011</p>
    </div>
    <a href="#comment-11274">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11274" class="permalink" rel="bookmark">And there doesn&#039;t seem to be</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>And there doesn't seem to be a way to select a subset via a single get/post request. It seems driven by server side session cookie params. This is hugely bad because it breaks the ability to scrape for people who need it.</p>
<p>Also, add a reverse dns (PTR) display option.</p>
<p>Love it. Thanks :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
