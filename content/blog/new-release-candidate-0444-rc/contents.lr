title: New release candidate: 0.4.4.4-rc
---
pub_date: 2020-08-13
---
author: nickm
---
tags: release candidate
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.4.4-rc from the <a href="https://www.torproject.org/download/tor/">download page</a>. Packages should be available over the coming weeks, with a new alpha Tor Browser release likely in the coming weeks.</p>
<p>Remember, this is a release candidate, not a a stable release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.4.4.4-rc is the first release candidate in its series. It fixes several bugs in previous versions, including some that caused annoying behavior for relay and bridge operators.</p>
<h2>Changes in version 0.4.4.4-rc - 2020-08-13</h2>
<ul>
<li>Minor features (security):
<ul>
<li>Channels using obsolete versions of the Tor link protocol are no longer allowed to circumvent address-canonicity checks. (This is only a minor issue, since such channels have no way to set ed25519 keys, and therefore should always be rejected for circuits that specify ed25519 identities.) Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40081">40081</a>.</li>
</ul>
</li>
<li>Minor features (defense in depth):
<ul>
<li>Wipe more data from connection address fields before returning them to the memory heap. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/6198">6198</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor bugfixes (correctness, buffers):
<ul>
<li>Fix a correctness bug that could cause an assertion failure if we ever tried using the buf_move_all() function with an empty input buffer. As far as we know, no released versions of Tor do this. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40076">40076</a>; bugfix on 0.3.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (linux seccomp2 sandbox):
<ul>
<li>Fix startup crash with seccomp sandbox enabled when tor tries to open the data directory. Patch from Daniel Pinto. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40072">40072</a>; bugfix on 0.4.4.3-alpha-dev.</li>
</ul>
</li>
<li>Minor bugfixes (onion service v3):
<ul>
<li>Remove a BUG() warning that could trigger in certain unlikely edge-cases. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/34086">34086</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (rate limiting, bridges, pluggable transports):
<ul>
<li>On a bridge, treat all connections from an ExtORPort as remote by default for the purposes of rate-limiting. Previously, bridges would treat the connection as local unless they explicitly received a "USERADDR" command. ExtORPort connections still count as local if there is a USERADDR command with an explicit local address. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33747">33747</a>; bugfix on 0.2.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relay, self-testing):
<ul>
<li>When starting up as a relay, if we haven't been able to verify that we're reachable, only launch reachability tests at most once a minute. Previously, we had been launching tests up to once a second, which was needlessly noisy. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40083">40083</a>; bugfix on 0.2.8.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>When running the subsystem order check, use the Python binary configured with the PYTHON environment variable. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40095">40095</a>; bugfix on 0.4.4.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (windows):
<ul>
<li>Fix a bug that prevented Tor from starting if its log file grew above 2GB. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/31036">31036</a>; bugfix on 0.2.1.8-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-289448"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289448" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 13, 2020</p>
    </div>
    <a href="#comment-289448">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289448" class="permalink" rel="bookmark">Tor NOTICE: Tor has been…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor NOTICE: Tor has been idle for 33223 seconds; assuming established circuits no longer work.<br />
Tor NOTICE: Heartbeat: Tor's uptime is 2 days 14:29 hours, with 5 circuits open. I've sent 36.41 MB and received 227.67 MB.<br />
Tor NOTICE: While not bootstrapping, fetched this many bytes:<br />
Tor NOTICE: 2548239 (consensus network-status fetch)<br />
Tor NOTICE: 396326 (microdescriptor fetch)<br />
Tor NOTICE: Average packaged cell fullness: 45.884%. TLS write overhead: 5%<br />
Tor WARN: Failed to find node for hop #1 of our path. Discarding this circuit.<br />
Tor NOTICE: Our circuit 0 (id: 3425) died due to an invalid selected path, purpose General-purpose client. This may be a torrc configuration issue, or a bug. [9 similar message(s) suppressed in last 3600 seconds]<br />
Tor WARN: Guard catmeme5 ($9A48EC8AAFB2E8C9E20408596383DB74F1196747) is failing an extremely large amount of circuits. This could indicate a route manipulation attack, extreme network overload, or a bug. Success counts are 6/166. Use counts are 1/1. 164 circuits completed, 0 were unusable, 158 collapsed, and 163 timed out. For reference, your timeout cutoff is 60 seconds.<br />
Tor NOTICE: No circuits are opened. Relaxed timeout for circuit 3442 (a Measuring circuit timeout 3-hop circuit in state waiting to see how other guards perform with channel state open) to 60000ms. However, it appears the circuit has timed out anyway.</p>
</div>
  </div>
</article>
<!-- Comment END -->
