title: Strength in Numbers: An Onion Blooms
---
pub_date: 2018-11-15
---
author: ssteele
---
tags:

Strength in Numbers
executive director
---
categories: jobs
---
summary:

Today I step down from the role of Executive Director of the Tor Project. I joined the organization three years ago, and I've had the privilege of being part of this special community as it has grown from its teenage years to full adulthood. I am so proud of all that we have accomplished in my short tenure.
 
---
_html_body:

<p> </p>
<p>Today I step down from the role of Executive Director of the Tor Project. I joined the organization three years ago, and I've had the privilege of being part of this special community as it has grown from its teenage years to full adulthood. We have become an important leader in the Internet Freedom space, and Tor Project employees, contractors, and volunteers are respected and sought after for their technical expertise and community organizing skills. I am so proud of all that we have accomplished in my short tenure.<br />
 <br />
Three years ago, when I started as Executive Director, the Tor Project employed nine people and retained another nine as full-time contractors. All of these people lived in North America or Europe. The organization's yearly budget was around $2.5 million USD, and almost all of that money came from various U.S. government contracts and grants.<br />
 <br />
Today, the Tor Project is an embodiment of the theme of our year-end fundraising campaign, "Strength in Numbers.” <a href="https://www.torproject.org/about/corepeople.html.en">The organization</a> now has 35 employees and 14 contractors across five continents. We have employees in South America and Africa, and more women have stepped into positions of leadership. Last fiscal year (July 1, 2017, through June 30, 2018), our yearly budget was $4.5 million USD, and nearly half of that money came from non-U.S. government sources.<br />
 <br />
The Tor technology has matured, as well. Tor Browser has become <a href="https://blog.torproject.org/new-release-tor-browser-80">easier to use</a> and is translated into more languages. We now have a <a href="https://blog.torproject.org/new-alpha-release-tor-browser-android">Tor Browser for Android</a>. We have optimized our core Tor software for mobile devices and for connecting from low-bandwidth environments. The Tor Network has become faster and more efficient.<br />
 <br />
Most of all, the Tor community has expanded and matured. We have a vibrant community of contributors who run the network, ponder hard problems, and embody the open source movement. We’ve established teams that work with one another on a daily basis. Our teams include the network and browser teams, as well as new teams for fundraising and usability and half a dozen others. Everyone is part of a larger group, all working to achieve common goals.</p>
<p>I’m so proud of the what the organization has become. But let’s be clear; we didn't do it alone.  Nonprofit organizations depend on the support of individuals like you, and an open source project like the Tor Project cannot survive without our contributors.  </p>
<p><a href="https://torproject.org/donate/donate-sin-ss-bp"><img alt="donate-button" src="/static/images/blog/inline-images/tor-donate-button_2.png" class="align-center" /></a></p>
<p>I've just donated $3,000, a thousand dollars for each of my years as ED. Won't you join me? There’s <a href="https://torproject.org/donate/donate-sin-ss-bp">Strength in Numbers</a>! If you donate before the end of 2018, Mozilla will match your donation (thank you, Mozilla!), and we'll send you some cool swag with our amazing "Anonymity Loves Company" design.</p>
<p>While this is a bit of a farewell, I’m not going very far; I will be joining the Tor Project’s Board of Directors starting in January. I look forward to supporting the Tor Project’s new Executive Director, <a href="https://blog.torproject.org/announcing-tors-next-executive-director-isabela-bagueros">Isabela Bagueros</a>, as she continues to help the organization grow and thrive.</p>
<p>Yours in Freedom,</p>
<p><a href="https://donate.torproject.org/donate/donate-sin-ss-bp"><img alt="Tor Seattle Office with Shari " src="/static/images/blog/inline-images/tor-project-shari-farewell.png" class="align-center" /></a></p>
<p>Shari<br />
Outgoing Executive Director<br />
The Tor Project, Inc.</p>

---
_comments:

<a id="comment-278486"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278486" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>a fan (not verified)</span> said:</p>
      <p class="date-time">November 15, 2018</p>
    </div>
    <a href="#comment-278486">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278486" class="permalink" rel="bookmark">thank you for everything…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thank you for everything Shari &lt;3 &lt;3 &lt;3</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278488"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278488" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 15, 2018</p>
    </div>
    <a href="#comment-278488">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278488" class="permalink" rel="bookmark">We need a million thanks for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We need a million thanks for each one of us to match the level of work you've put into the Tor Project, we wis for you a long and happy life!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278502"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278502" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Denpafighter978VGCP (not verified)</span> said:</p>
      <p class="date-time">November 16, 2018</p>
    </div>
    <a href="#comment-278502">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278502" class="permalink" rel="bookmark">Awesome!!! Where can i buy a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Awesome!!! Where can i buy a shirt? This is a revolution in internet privacy!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278520"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278520" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  steph
  </article>
    <div class="comment-header">
      <p class="comment__submitted">steph said:</p>
      <p class="date-time">November 19, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278502" class="permalink" rel="bookmark">Awesome!!! Where can i buy a…</a> by <span>Denpafighter978VGCP (not verified)</span></p>
    <a href="#comment-278520">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278520" class="permalink" rel="bookmark">You can get a shirt when you…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can get a shirt when you make a donation! Click the donate button in the post.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278503"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278503" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 16, 2018</p>
    </div>
    <a href="#comment-278503">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278503" class="permalink" rel="bookmark">You guys are doing something…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You guys are doing something truly beautiful</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278505"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278505" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>tor in Uganda  (not verified)</span> said:</p>
      <p class="date-time">November 17, 2018</p>
    </div>
    <a href="#comment-278505">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278505" class="permalink" rel="bookmark">great job well done,  i have…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>great job well done,  i have donated 100 US  dollars to anonymity</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278519"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278519" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  steph
  </article>
    <div class="comment-header">
      <p class="comment__submitted">steph said:</p>
      <p class="date-time">November 19, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278505" class="permalink" rel="bookmark">great job well done,  i have…</a> by <span>tor in Uganda  (not verified)</span></p>
    <a href="#comment-278519">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278519" class="permalink" rel="bookmark">Thank you for joining us!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for joining us!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278517"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278517" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anonymous (not verified)</span> said:</p>
      <p class="date-time">November 19, 2018</p>
    </div>
    <a href="#comment-278517">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278517" class="permalink" rel="bookmark">keep up the good work!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>keep up the good work!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278526"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278526" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2018</p>
    </div>
    <a href="#comment-278526">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278526" class="permalink" rel="bookmark">I&#039;d like to offer heartfelt…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'd like to offer heartfelt thanks to Shari Steele for her hard behind-the-scenes work in beginning the much needed transformation of Tor Project from something too closely resembling a "soft-power" catspaw of the USG into a user-funded grassroots global project not beholden to any government, with the financial, technical, and human resources to globally resist attempts by any government (especially USG) to declare all strong (which means non-backdoored) civilian cryptography illegal, which would make individual freedom itself illegal.</p>
<p>I am heartened to hear that Shari will serve on the Board of Directors and hope we users can take this as a sign that Tor Project remains committed to the user-funded non-government-influenced vision of the Project as an independent globally dispersed organization which uses independent globally dispersed technology to promote free speech, uncensored access to information, and respect for human rights.</p>
<p>Hate crimes.  Illegal land grabs.  Attacks on journalists.  On eco-defenders.  On the rule of law.  Invasions.  The shelling and bombing of cities.  Religious persecution.  Terrorism.  Inflaming ethnic hatred.  Incitement of genocide.  Some things are too awful to be forced into bipolar categories such as "right" or "wrong".  To the American government we must say: yes, terrorism was wrong when AQ did it, but terrorism does not magically become "just" when the American government does it.  To the Russian government we must say: yes, invasion was wrong when the USA invaded the Soviet Union, but did not become "right" when Russia invaded Ukraine.  Cyberattack on a power grid in dead of winter is wrong even when it is not NSA but GRU which does it.  To the Chinese government we must say that, yes, interning millions of innocent civilians in huge detention camps was wrong when the USG did it (during World War II), but it is also wrong when the Chinese government does it (right now, in Xinjiang Province).  And so on and so on and so on.   Some things are just plain wrong, and it matters not a whit who does them.</p>
<p>Increasingly, as all the worlds governments turn as one to embrace to fascism, it seems that all governments are committing more and more unimaginable evils in as many places as they can, as fast as they can.   That's why all we endangered humans need to support organizations like Tor Project, so that we can attempt to speak out against evil acts, regardless of which government is responsible.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278601"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278601" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Sumana Harihareswara (not verified)</span> said:</p>
      <p class="date-time">November 26, 2018</p>
    </div>
    <a href="#comment-278601">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278601" class="permalink" rel="bookmark">Thank you for your work!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for your work!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278870"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278870" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>parrybarry (not verified)</span> said:</p>
      <p class="date-time">December 11, 2018</p>
    </div>
    <a href="#comment-278870">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278870" class="permalink" rel="bookmark">Just wanted to say to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just wanted to say to EVERYONE AT TOR DEVELOPMENT, thank you. your browser literally has saved my life.</p>
</div>
  </div>
</article>
<!-- Comment END -->
