title: Save Open Technology Fund, #SaveInternetFreedom
---
pub_date: 2020-06-24
---
author: isabela
---
summary:

The Tor Project has joined the voices around the world from the internet freedom community and in the U.S. Congress to express concerns about the rapid firing of key personnel and dissolution of the board of directors at the four agencies (Middle East Broadcasting, Radio Free Asia, Radio Free Europe/Radio Liberty, and the Open Technology Fund) under the U.S. Agency for Global Media (USAGM). 
---
_html_body:

<p>The Tor Project has joined the voices around the world from the internet freedom community and in the U.S. Congress to express concerns about the rapid firing of key personnel and dissolution of the board of directors at the four agencies (Middle East Broadcasting, Radio Free Asia, Radio Free Europe/Radio Liberty, and the Open Technology Fund) under the U.S. Agency for Global Media (USAGM).</p>
<p>Of most immediate concern to Tor is the future of the Open Technology Fund (OTF) and its crucial mission, since 2012, of providing funding for technology that enables free expression, helps people circumvent censorship, and obstructs repressive surveillance.</p>
<p>The Tor Project and OTF both recognize that for any strategy to provide censorship circumvention and surveillance protection to really be successful for the long term, it has to be an open, decentralized approach. We both know that it’s vital for the user to trust the tool in order to use it, and we both agree that the best way to build trust is by being open and allowing third party validation of your tool.</p>
<p>Since 2012, OTF has developed a method for grantmaking that has gained the trust of the global community. OTF ensures trust in the tools they support by sponsoring third-party security audits and bug bounty programs for their grantees and by following an ethical and rigorously transparent funding process. With this approach, OTF has grown a portfolio of open source solutions for censorship circumvention, surveillance protections, and overall privacy enhancements that benefit over 2 billion people globally.</p>
<p>The Tor Project has been a grantee of OTF since 2012 and we are very thankful for what we have been able to achieve with their support. We are thrilled that OTF has created a community of projects that work together, not just isolated, singular tools. Like OTF, we build trust with our users by being transparent about our processes and making sure our code is open and accessible for anyone to review. We know that openness and transparency are effective strategies, not only because our methods help millions of daily Tor users to get online safely, but also because our work, including pluggable transport methodologies and onion services, has helped other applications (both open and closed source) in our ecosystem to provide better circumvention and security features to their users.</p>
<p>The abrupt changes under USAGM, especially the firing of key OTF leadership, raises concerns regarding the future of a core supporter of the internet freedom community.</p>
<p><strong>We have signed an open letter asking members of Congress to:</strong></p>
<ul>
<li>Require USAGM to honor existing FY2019 and FY2020 spending plans to support the Open Technology Fund;</li>
<li>Require all US-Government internet freedom funds to be awarded via an open, fair, competitive, and evidence-based decision process;</li>
<li>Require all internet freedom technologies supported with US-Government funds to remain fully open-source in perpetuity;</li>
<li>Require regular security audits for all internet freedom technologies supported with US-Government funds; and</li>
<li>Pass the Open Technology Fund Authorization Act.</li>
</ul>
<p>On the top of that, we would like to add a request directly to the new administration at OTF.</p>
<p><strong>We strongly urge that the new administration of OTF takes the history of the organization very seriously and:</strong></p>
<ul>
<li>Keep the remaining staff;</li>
<li>Honor current OTF contracts with the internet freedom community; and</li>
<li>Guarantee that current OTF staff can continue their grantmaking processes according to the existing method that has proven successful in the battle to promote human rights and open societies.</li>
</ul>
<p>The Open Technology Fund has almost a decade of history supporting organizations and individuals all around the world in their mission of ensuring billions of people have secure access to the internet. This is the internet freedom community. Everyone who reads this blog post knows how crucial the internet is for them, especially as we live this moment of physical isolation due to the COVID-19 pandemic. Today, more than ever, we need to ensure that initiatives like OTF are out there to support our internet freedom community and its mission.</p>
<p>Help us save OTF. You can join us and sign the letter, either as an individual or an organization, at <a href="https://saveinternetfreedom.tech/information/how-to-help/">https://saveinternetfreedom.tech/information/how-to-help/</a></p>

