title: TorBEL: The Tor Bulk Exit List Tools
---
pub_date: 2010-08-20
---
author: hbock
---
tags:

gsoc2010
torbel
---
_html_body:

<p>We have finally crossed the point of no return: 16 August 2010, 1900 UTC.  Google Summer of Code 2010 is over; it's been a lot of fun and a lot of hard work participating in GSoC this year, and I hope that I have accomplished my goal.  I took up the task of redesigning <a href="https://exitlist.torproject.org" rel="nofollow">TorDNSEL</a>, a DNSBL-style interface for querying information about Tor exit nodes, to be more thorough, more usable, and more maintainable.  Out of this effort came TorBEL, a set of specifications and Python tools that try to address this problem.</p>

<p>  The TorBEL codebase as released today contains several important<br />
  pieces:</p>

<ul>
<li>
      A more thorough <a href="http://git.spanning-tree.org/index.cgi/torbel/tree/doc/test-spec.txt?id=torbel-0.1-gsoc" rel="nofollow">specification</a> for active testing of the Tor network to determine fine-grained reachability through exit relays
    </li>
<li>
      A <a href="http://git.spanning-tree.org/index.cgi/torbel/tree/doc/data-spec.txt?id=torbel-0.1-gsoc" rel="nofollow">specification</a> for the export of this data in bulk to consumers, in CSV and JSON formats and via the DNSEL blocklist interface.
    </li>
<li>
      A high-throughput implementation of the active testing scheme using the <a href="http://twistedmatrix.com/trac/" rel="nofollow">Twisted</a> framework (torbel)
    </li>
<li>
      A Python API (torbel.query) to easily query against these exports.
    </li>
<li>
      An implementation of a DNSEL server (torbel.dnsel) written on top of twisted.names compatible with the original tordnsel that supports more query types and uses TorBEL's export formats and query API.
    </li>
</ul>

<p>Although TorBEL is relatively stable at this point and does its job well, providing better answers where the current DNSEL is not accurate and providing an easy way for others to generate and consume exit node data, it is far from finished.  Bugs need fixing and test accuracy can be improved further still, and TorBEL will need to face the test of running on a production system, used by real-world consumers like OFTC and FreeNode.  I fully intend to stay with the Tor project as a volunteer developer and maintain TorBEL as long as it needs maintaining.</p>

<p>I'd like to thank Sebastian Hahn for being a fantastically helpful mentor and for sticking with me even through some frustrating late nights and my lack of regular communication. Sebastian has pushed me harder than I expected to meet my goals on time and to revise them when I could not do so.  Working with me from another part of the world with a six-hour time difference made things interesting, although insomnia played a key role in keeping us in contact.  I can be terribly difficult to get in touch with and it really didn't help working another part-time job for the entire coding duration.  This caused a few problems, as Sebastian tried very hard to encourage me to work in the open and communicate often, which I often failed to do. I will try to improve my communications with Tor project members in the future.</p>

<p>In addition to Sebastian, I'd like to thank the many kind members of the Tor community for reaching out and helping me get up to speed and resolve many areas I had trouble with, including Nick Matthewson, Roger Dingledine, Mike Perry, Damian Johnson, and Ilja Hallberg.  Without their unending help and resourcefulness I would have been stuck long ago.</p>

<p>All in all GSoC 2010 has been a great experience and I really hope to see that TorBEL gets some real-world usage in the very near future.  I very much look forward to continuing work with the Tor developer community on TorBEL and hopefully other interesting projects as well.</p>

---
_comments:

<a id="comment-7270"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7270" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 20, 2010</p>
    </div>
    <a href="#comment-7270">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7270" class="permalink" rel="bookmark">Hi,
Thanks for your time on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,</p>
<p>Thanks for your time on this project and great to see that you has enjoy your work with all difficulty that included..</p>
<p>my best and keep up continue the TorBEL :)</p>
<p>SwissTorExit</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7494"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7494" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 22, 2010</p>
    </div>
    <a href="#comment-7494">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7494" class="permalink" rel="bookmark">Haven&#039;t  got it figured out</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Haven't  got it figured out yet, but you need a big pat on the back for what it sounds like you put a lot of time into....</p>
<p>busts44</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7495"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7495" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 22, 2010</p>
    </div>
    <a href="#comment-7495">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7495" class="permalink" rel="bookmark">thanks  for every thing my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanks  for every thing my friend.</p>
<p>appreciated all of your time and hard work .</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7507"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7507" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 26, 2010</p>
    </div>
    <a href="#comment-7507">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7507" class="permalink" rel="bookmark">is there any chance 2 make</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>is there any chance 2 make tork in windows also? Please answer</p>
</div>
  </div>
</article>
<!-- Comment END -->
