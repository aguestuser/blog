title: Tor Weekly News — May 6th, 2015
---
pub_date: 2015-05-06
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the eighteenth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor Project, Inc. appoints Interim Executive Director</h1>

<p>Following the <a href="https://blog.torproject.org/blog/roger-dingledine-becomes-interim-executive-director-tor-project" rel="nofollow">departure</a> of the Tor Project, Inc.’s Executive Director, Andrew Lewman, the <a href="https://www.torproject.org/about/board" rel="nofollow">board of directors</a> has appointed Roger Dingledine as Interim Executive Director, and Nick Mathewson as Interim Deputy Executive Director, until long-term candidates for these roles are found. Roger and Nick are both co-founders and lead developers of Tor, and need no introduction here — but you can watch Roger’s <a href="https://www.nsf.gov/news/mmg/mmg_disp.jsp?med_id=77516" rel="nofollow">conversation</a> with the National Science Foundation and (if you read Spanish) take a look at Nick’s recent <a href="http://tecnologia.elpais.com/tecnologia/2015/04/04/actualidad/1428169979_196077.html" rel="nofollow">interview</a> with El País to learn a bit more about who they are and what inspires them to work on Tor.</p>

<h1>Monthly status reports for April 2015</h1>

<p>The wave of regular monthly reports from Tor project members for the month of April has begun. <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000807.html" rel="nofollow">George Kadianakis</a> released his report first (offering updates on onion service research), followed by reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000808.html" rel="nofollow">Yawning Angel</a> (reporting on pluggable transport research and core Tor hacking), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000809.html" rel="nofollow">Sherief Alaa</a> (on support work, documentation rewrites, and testing), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000810.html" rel="nofollow">David Goulet</a> (on onion service and core Tor development), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000811.html" rel="nofollow">Nick Mathewson</a> (on core Tor development and organizational work), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000812.html" rel="nofollow">Leiah Jansen</a> (on graphic design and branding), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000813.html" rel="nofollow">Pearl Crescent</a> (on Tor Browser and Tor Launcher development and testing), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000814.html" rel="nofollow">Jacob Appelbaum</a> (on advocacy and outreach), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000815.html" rel="nofollow">Griffin Boyce</a> (on security research and Satori/Cupcake development), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000816.html" rel="nofollow">Damian Johnson</a> (on Stem development and coordinating Tor Summer of Privacy), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000817.html" rel="nofollow">Georg Koppen</a> (on Tor Browser Development and build system research), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000819.html" rel="nofollow">Juha Nurmi</a> (on ahmia.fi development and Tor outreach), and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000820.html" rel="nofollow">Israel Leiva</a> (on the GetTor project).</p>

<p>Mike Perry reported on behalf of the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000818.html" rel="nofollow">Tor Browser team</a>, giving details of the 4.5 release process, significant security enhancements, and work to ensure that the wider Internet community takes the Tor network into account when developing standards and protocols.</p>

<h1>Miscellaneous news</h1>

<p>Isis Lovecruft <a href="https://lists.torproject.org/pipermail/tor-dev/2015-May/008760.html" rel="nofollow">announced</a> the release and deployment of version 0.3.2 of <a href="https://bridges.torproject.org" rel="nofollow">BridgeDB</a>, the software that handles bridge address collection and distribution for the Tor network. Notable changes include the setting of obfs4 as the default pluggable transport served to users, better handling of clients from the same IPv6 address block, and the exclusion of broken bridge lines from the database.</p>

<p>Tom Ritter shared a <a href="https://lists.torproject.org/pipermail/tor-talk/2015-May/037723.html" rel="nofollow">slide deck</a> offering “a 100-foot overview on Tor”: “Before I post it on twitter or a blog, I wanted to send it around semi-publicly to collect any feedback people think is useful.”</p>

<p>Moritz Bartl <a href="https://lists.torproject.org/pipermail/tor-talk/2015-April/037649.html" rel="nofollow">announced</a> the Tor-BSD Diversity Project, which aims to mitigate the risks that the “overwhelming GNU/Linux monoculture” among Tor relay operators might pose to the security of the Tor network: “In a global anonymity network, monocultures are potentially disastrous. A single kernel vulnerability in GNU/Linux that impacting Tor relays could be devastating. We want to see a stronger Tor network, and we believe one critical ingredient for that is operating system diversity.”</p>

<p>David Fifield published the regular <a href="https://lists.torproject.org/pipermail/tor-dev/2015-May/008767.html" rel="nofollow">summary of costs</a> incurred by the infrastructure for meek in April, detailing a large increase in simultaneous users over the last month (from 2000 to 5000), and the possible effects of a larger meek userbase on the <a href="https://metrics.torproject.org" rel="nofollow">Tor Metrics portal</a>’s bridge user graphs.</p>

<p>John Brooks <a href="https://lists.torproject.org/pipermail/tor-dev/2015-April/008743.html" rel="nofollow">suggested</a> that, when the “<a href="https://gitweb.torproject.org/torspec.git/plain/proposals/224-rend-spec-ng.txt" rel="nofollow">next-generation onion services</a>” proposal is implemented, there will no longer be any reason to use both <a href="https://www.torproject.org/docs/hidden-services" rel="nofollow">introduction points and hidden service directories</a> when establishing connections between Tor clients and onion services. Calculating introduction points in the same way that HSDirs would be selected may have “substantial” benefits: “Services touch fewer relays and don’t need to periodically post descriptors. Client connections are much faster. The set of relays that can observe popularity is reduced. It’s more difficult to become the IP of a targeted service.” See John’s proposal for a detailed explanation, and feel free to send your comments to the tor-dev mailing list.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony, Roger Dingledine, and Karsten Loesing.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

