title: Internet Freedom, Privacy, & LGBTQIA+ Human Rights
---
pub_date: 2021-06-14
---
author: alsmith
---
tags: pride
---
categories: human rights
---
summary: Every June, we recognize Pride month because internet freedom and the human rights of LGBTQIA+ people go hand in hand. Many LGBTQIA+ people need privacy and censorship circumvention tools like Tor to communicate with their peers, find important resources, or fight for their rights without facing violence.
---
_html_body:

<p>Every June, we recognize Pride month because internet freedom and the human rights of LGBTQIA+ people go hand in hand.</p>
<p>LGBTQIA+ people have the right to access information, resources, and community relevant to their identities without being tracked, surveilled, censored, or persecuted—but in many parts of the world, governments block LGBTQIA+ content and punish people who engage with it.</p>
<ul>
<li aria-level="1">In Russia, <a href="https://freedomhouse.org/country/russia/freedom-net/2020">people face fines, prison sentences, intimidation, and violence for posting LGBTQIA+ content</a>. In 2020, an activist was fined 75,000 rubles for posting a photo with the words, "Family is where the Love is. Support LGBT+ families." </li>
<li aria-level="1">In Indonesia, <a href="https://freedomhouse.org/country/indonesia/freedom-net/2020">LGBTQIA+ content, services, apps, and even emojis depicting LGBTQIA+ themes are frequently blocked</a> or removed from app stores, with big tech complying with demands to censor this content. In 2018, <a href="https://www.haaretz.com/israel-news/.premium.MAGAZINE-israel-s-cyber-spy-industry-aids-dictators-hunt-dissidents-and-gays-1.6573027">Indonesian authorities purchased surveillance tools to track LGBTQIA+ rights activists</a>.</li>
<li aria-level="1">In Uganda, <a href="https://freedomhouse.org/country/uganda/freedom-net/2020">the LGBTQIA+ community is the target of regular technical attacks</a>. Activists are hacked and surveilled, and attackers use content from hacked accounts to blackmail LGBTQIA+ people in the spotlight. </li>
<li aria-level="1">In the United States, public schools are required by federal law to use software to block pornographic websites, but <a href="https://www.aclu.org/issues/lgbtq-rights/lgbtq-youth/anti-lgbtq-web-filtering?redirect=issues/lgbt-rights/lgbt-youth/anti-lgbt-web-filtering">schools often use that software to block sites that discuss LGBTQIA+ issues and resources that are not sexually explicit</a> in any way.</li>
<li aria-level="1">In Egypt, <a href="https://edri.org/our-work/the-digital-rights-lgbtq-technology-reinforces-societal-oppressions/">authories used the social networking app Grindr to track down and persecute LGBTQIA+ people</a> by using fake profiles to gather evidence that is used to imprison, torture, and prosecute LGBTQIA+ people for “illegal sexual behavior.”</li>
</ul>
<p>Globally, many LGBTQIA+ people need privacy and censorship circumvention tools like Tor to communicate with their peers, find important resources, or fight for their rights without facing violence.</p>
<p>This is one of the many reasons why we do what we do at the Tor Project, and it’s the reason we monitor the availability of LGBTQ+ sites in<a href="https://ooni.torproject.org/"> OONI</a> tests, so we can better understand which countries are censoring these sites and who needs circumvention technology. (You can help monitor internet censorship, including against LGBTQIA+ sites, by running <a href="https://ooni.org/install/">OONI Probe</a>.) That’s why we travel to countries where governments outlaw or punish being LGBTQIA+ and lead workshops for community organizations about how to protect their privacy online. That’s why we partner with LGBTQIA+ groups to ensure that we’re learning about how they use privacy tech, and what they need from Tor in order to stay safe when using the internet.</p>
<p>Pride and privacy go hand in hand. This June and year round, the Tor Project stands in solidarity with the LGBTQIA+ community. Happy Pride! </p>

