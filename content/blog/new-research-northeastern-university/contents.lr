title: The New Research from Northeastern University
---
pub_date: 2016-07-25
---
author: arma
---
tags:

onion services
Northeastern University
bad relays
---
categories: onion services
---
_html_body:

<p>We’ve been speaking to journalists who are curious about a <a href="https://www.securityweek2016.tu-darmstadt.de/pets-2016/hotpets/" rel="nofollow">HotPETS 2016</a> talk from last week: the <a href="https://www.securityweek2016.tu-darmstadt.de/fileadmin/user_upload/Group_securityweek2016/pets2016/10_honions-sanatinia.pdf" rel="nofollow">HOnions: Towards Detection and Identification of Misbehaving Tor HSDirs</a> research paper conducted by our colleagues at Northeastern University. Here's a short explanation, written by Donncha and Roger.</p>

<p>Internally, Tor has a system for identifying <a href="https://trac.torproject.org/projects/tor/wiki/doc/ReportingBadRelays" rel="nofollow">bad relays</a>. When we find a bad relay, we throw it out of the network.</p>

<p>But our techniques for finding bad relays aren't perfect, so it's good that there are other researchers also working on this problem. Acting independently, we had already detected and removed many of the suspicious relays that these researchers have found.</p>

<p>The researchers have sent us a list of the other relays that they found, and we're currently working on confirming that they are bad. (This is tougher than it sounds, since the technique used by the other research group only detects that relays *might* be bad, so we don't know which ones to blame for sure.)</p>

<p>It's especially great to have this other research group working on this topic, since their technique for detecting bad relays is different from our technique, and that means better coverage.</p>

<p>As far as we can tell, the misbehaving relays' goal in this case is just to discover onion addresses that they wouldn't be able to learn other ways—they aren't able to identify the IP addresses of hosts or visitors to Tor hidden services.</p>

<p>The authors here are not trying to discover new onion addresses. They are trying to detect other people who are learning about onion addresses by running bad HSDirs/relays.</p>

<p>This activity only allows attackers to discover new onion addresses. It does not impact the anonymity of hidden services or hidden service clients.</p>

<p>We have known about and been defending against this situation for quite some time. The issue will be resolved more thoroughly with the next-generation hidden services design. Check out our blog post, <a href="https://blog.torproject.org/blog/mission-montreal-building-next-generation-onion-services" rel="nofollow">Mission: Montreal!</a></p>

---
_comments:

<a id="comment-194944"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-194944" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 26, 2016</p>
    </div>
    <a href="#comment-194944">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-194944" class="permalink" rel="bookmark">Always protect your private</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Always protect your private onions, ssh onions, etc. with HiddenServiceAuthorizeClient and HidServAuth. The bad HSDirs will still see your onion address, but they won't be able to access it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-195398"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-195398" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 28, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-194944" class="permalink" rel="bookmark">Always protect your private</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-195398">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-195398" class="permalink" rel="bookmark">&gt; The bad HSDirs will still</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; The bad HSDirs will still see your onion address,</p>
<p>Do all Tails users have an onion address (e.g. for Whisperback?).</p>
<p>As far as I know I am not running an onion service, but in the last month I have seen a good deal of what appear to me (as an experienced but noncoding Tor user) to suggest nefarious activity which sounds similar to what I have been reading about bad guys abusing HS to deanonymize Tor users.</p>
<p>@ Tor/Tails people: any information, suggestions, or links would be appreciated.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-195019"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-195019" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 26, 2016</p>
    </div>
    <a href="#comment-195019">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-195019" class="permalink" rel="bookmark">what to do AS USERS to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>what to do AS USERS to "help" a bit" that mission montreal be a full success (for every one) in a near future ?<br />
-stopping using compromised service or o.s (which ones must be avoid )?<br />
-choosing/replacing safe server vpn:dns  (which ones must be avoid ) ?<br />
-adding relays (is it more predictive or less _ less or more trust _ ) ?<br />
-using more tor (24/24 )?<br />
-adding more users (spreading the world_ convincing more people) ?<br />
-buying a laptop or a pi for a right entropy (should it be better with a chip faster_arm or a chip with virtual feature enable , with one or two gpu ) ?<br />
-using 2 laptop/rasperry , one as server (protective personal first barrier/relay) and one as user ?<br />
-creating a firewall which should eliminate 'bad' relays or putting it in a temporary black list ?<br />
-building a special card with embedded chip/program or making/downloading a special program to increase the good statistics values (or decrease the bad) ?<br />
Is the user still staying a spectator of tor project ?<br />
*the new intel chip is compromised, ubuntu too (backdoor &amp; new laws) , nvidia card too ... will it compromise the usage of tor ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-195397"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-195397" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 28, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-195019" class="permalink" rel="bookmark">what to do AS USERS to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-195397">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-195397" class="permalink" rel="bookmark">&gt; *the new intel chip is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; *the new intel chip is compromised, ubuntu too (backdoor &amp; new laws) , nvidia card too ... will it compromise the usage of tor ?</p>
<p>Citation?  Details?  Which chips and cards precisely?  (Year, model, name).  Compromised by whom?  China?  NSA?  Intel itself?  Compromised in what sense?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-195563"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-195563" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 29, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-195019" class="permalink" rel="bookmark">what to do AS USERS to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-195563">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-195563" class="permalink" rel="bookmark">help yourself not using any</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>help yourself not using any closed source/commercial os and let windowz geeks be tracked and cracked by THE GREAT DEMOCRATIC COUNTRY LAW - they want it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-195164"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-195164" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 27, 2016</p>
    </div>
    <a href="#comment-195164">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-195164" class="permalink" rel="bookmark">Been getting a lot of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Been getting a lot of phishing relays that redirect you and then it goes to a cloned government phishing server or site and steals your info or de-anonymizes you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-195403"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-195403" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 28, 2016</p>
    </div>
    <a href="#comment-195403">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-195403" class="permalink" rel="bookmark">OT, but relevant and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>OT, but relevant and ironical anecdote:</p>
<p>Civil libertarians often need to obtain official government datasets, often from some US government agency, sometimes from hostile agencies such as FBI which offer information to the public (even outside the USA) at the fbi.gov public website.  But it is important to know you are connecting to the genuine website and not an impostor.  Fortunately, most government websites use https.  On the other hand, civil libertarians don't want to connect to a hostile government agency directly because we know better than most people that FBI is likely to start dangerously wrong guesses about why someone wants a copy of a public dataset.  So naturally we use Tor.  Which we use for everything anyway, because we know the Internet has become a very dangerous place (thank you NSA, and all such like that).</p>
<p>And if you connect to fbi.gov using Tor Browser, what happens?  Why cloudflare happens, of course!  And what do you see in Tor Browser's location pane?  fbi.gov with a lock icon.  But when you click to look at the certificate, you discover this is owned not by fbi.gov but by cloudflare, and the "Subject" line does not even mention FBI.</p>
<p>Doesn't this trickery subvert the very purpose of PKI?</p>
<p>Lesson: always check the certificate itself.  It it is owned by cloudflare and makes no mention of the site you think you are connected to, you are not connected to the site you probably think you are.  This opens the door to abuse by unknown parties.</p>
<p>See also a current thread in tor-talk about cloudflare being abused to deanonymize Tor users by requiring them to click on images.  Doing that is not only dangerous (yes?), it won't work.  In my experience, Tor users are never able to pass the bar, cloudflare makes them click until (presumably) they are deanonymized, but even then, cloudflare prevents them from accessing the promised public data.</p>
<p>Not all USG agencies are as tricky as FBI, of course.  In my experience some other USG agency websites do provide public data even to Tor users without cloudflare blocks.  Same for agencies of many EU governments.  But FBI never did provide the promised public dataset.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-195406"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-195406" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 28, 2016</p>
    </div>
    <a href="#comment-195406">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-195406" class="permalink" rel="bookmark">Should I see the same node X</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Should I see the same node X in two places in two adjacent circuits listed in Onion Circuits?<br />
Like so:<br />
A X B<br />
C D X<br />
(relay and exit positions in two adjacent circuits to be used one right after the other, potentially).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-195721"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-195721" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 30, 2016</p>
    </div>
    <a href="#comment-195721">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-195721" class="permalink" rel="bookmark">Tcpcrypt - Encrypting the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tcpcrypt - Encrypting the Internet<br />
<a href="http://tcpcrypt.org/download.php" rel="nofollow">http://tcpcrypt.org/download.php</a></p>
<p>PLEASE IMPLEMENT IN NEW VERSIONS?</p>
</div>
  </div>
</article>
<!-- Comment END -->
