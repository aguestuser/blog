title: Tor 0.2.9.7-rc is released: almost stable!
---
pub_date: 2016-12-12
---
author: nickm
---
tags:

tor
release
release candidate
---
categories:

network
releases
---
_html_body:

<p>There's a new development release of Tor!<br />
Tor 0.2.9.7-rc fixes a few small bugs remaining in Tor 0.2.9.6-rc, including a few that had prevented tests from passing on some platforms.<br />
The source code for this release is now available from the <a href="https://www.torproject.org/download/download" rel="nofollow">download page</a> on our website. Packages should be available soon.  I expect that this Tor release will probably go into the hardened TorBrowser package series coming out in the next couple of days. (I hear that 0.2.9.6-rc  will be in the regular TorBrowser alphas, since those froze a little  before I finished this Tor release.)<br />
We're rapidly running out of serious bugs to fix in 0.2.9.x, so this is probably the last release candidate before stable ... unless you find bugs while testing!  Please try these releases, and let us know if anything breaks.  Testing either 0.2.9.6-rc or 0.2.9.7-rc would be helpful.</p>

<h2>Changes in version 0.2.9.7-rc - 2016-12-12</h2>

<ul>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the December 7 2016 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor bugfix (build):
<ul>
<li>The current Git revision when building from a local repository is now detected correctly when using git worktrees. Fixes bug <a href="https://bugs.torproject.org/20492" rel="nofollow">20492</a>; bugfix on 0.2.3.9-alpha.
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Minor bugfixes (directory authority):
<ul>
<li>When computing old Tor protocol line version in protover, we were looking at 0.2.7.5 twice instead of a specific case for 0.2.9.1-alpha. Fixes bug <a href="https://bugs.torproject.org/20810" rel="nofollow">20810</a>; bugfix on tor-0.2.9.4-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (download scheduling):
<ul>
<li>Resolve a "bug" warning when considering a download schedule whose delay had approached INT_MAX. Fixes <a href="https://bugs.torproject.org/20875" rel="nofollow">20875</a>; bugfix on 0.2.9.5-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>Downgrade a harmless log message about the pending_entry_connections list from "warn" to "info". Mitigates bug <a href="https://bugs.torproject.org/19926" rel="nofollow">19926</a>.
  </li>
</ul>
</li>
<li>Minor bugfixes (memory leak):
<ul>
<li>Fix a small memory leak when receiving AF_UNIX connections on a SocksPort. Fixes bug <a href="https://bugs.torproject.org/20716" rel="nofollow">20716</a>; bugfix on 0.2.6.3-alpha.
  </li>
<li>When moving a signed descriptor object from a source to an existing destination, free the allocated memory inside that destination object. Fixes bug <a href="https://bugs.torproject.org/20715" rel="nofollow">20715</a>; bugfix on tor-0.2.8.3-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (memory leak, use-after-free, linux seccomp2 sandbox):
<ul>
<li>Fix a memory leak and use-after-free error when removing entries from the sandbox's getaddrinfo() cache. Fixes bug <a href="https://bugs.torproject.org/20710" rel="nofollow">20710</a>; bugfix on 0.2.5.5-alpha. Patch from "cypherpunks".
  </li>
</ul>
</li>
<li>Minor bugfixes (portability):
<ul>
<li>Use the correct spelling of MAC_OS_X_VERSION_10_12 on configure.ac Fixes bug <a href="https://bugs.torproject.org/20935" rel="nofollow">20935</a>; bugfix on 0.2.9.6-rc.
  </li>
</ul>
</li>
<li>Minor bugfixes (unit tests):
<ul>
<li>Stop expecting NetBSD unit tests to report success for ipfw. Part of a fix for bug <a href="https://bugs.torproject.org/19960" rel="nofollow">19960</a>; bugfix on 0.2.9.5-alpha.
  </li>
<li>Fix tolerances in unit tests for monotonic time comparisons between nanoseconds and microseconds. Previously, we accepted a 10 us difference only, which is not realistic on every platform's clock_gettime(). Fixes bug <a href="https://bugs.torproject.org/19974" rel="nofollow">19974</a>; bugfix on 0.2.9.1-alpha.
  </li>
<li>Remove a double-free in the single onion service unit test. Stop ignoring a return value. Make future changes less error-prone. Fixes bug <a href="https://bugs.torproject.org/20864" rel="nofollow">20864</a>; bugfix on 0.2.9.6-rc.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-224840"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224840" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 12, 2016</p>
    </div>
    <a href="#comment-224840">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224840" class="permalink" rel="bookmark">When we are going to have an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When we are going to have an official Tor browser for Ios? And until then what is the best option available right now for free(the most secure and trustfull). i just want to use tor for normal browsing(Im not interested in onion sites and  bad things) i just want a secure and private browsing experience. In 2015 i heard about project Icepa...an official tor browser that is also a vpn  but we are in 2016 and it not released yet. When is going to be available? If i use tor just for normal browsing it is possible to get hacked or get viruses ?To finish everything i want to add that i think what you are doing is very good for the human rights of privacy and it  sad that some people use this rights for doing bad things. Sorry if i miss spelled something  , im not very good at english</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224896"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224896" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 12, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224840" class="permalink" rel="bookmark">When we are going to have an</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224896">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224896" class="permalink" rel="bookmark">Just last week we got Mike</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just last week we got Mike Tigas to write a guest blog post on exactly this topic. You can read it here:<br />
<a href="https://blog.torproject.org/blog/tor-heart-onion-browser-and-more-ios-tor" rel="nofollow">https://blog.torproject.org/blog/tor-heart-onion-browser-and-more-ios-t…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-224912"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224912" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 12, 2016</p>
    </div>
    <a href="#comment-224912">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224912" class="permalink" rel="bookmark">What&#039;s the summary of what</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What's the summary of what is new and exciting in 0.2.9.x? There is the secure random number generation ability, right?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-225272"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225272" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">December 14, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224912" class="permalink" rel="bookmark">What&#039;s the summary of what</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-225272">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225272" class="permalink" rel="bookmark">We&#039;ll be doing new blog</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We'll be doing new blog posts on that as we put out a stable release. For now, I'm most impressed with the shared-random stuff; with the hidden service (and non-hidden service) performance improvements; with improved support for other implementations of the tor protocols; and with all our various performace and security improvements. </p>
<p>You can see a draft summary <a href="https://gitweb.torproject.org/tor.git/tree/ReleaseNotes.029?h=release-0.2.9" rel="nofollow">over here</a>, but it needs more editing to put the important stuff first.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-224942"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224942" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 13, 2016</p>
    </div>
    <a href="#comment-224942">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224942" class="permalink" rel="bookmark">could you check if Turkey</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>could you check if Turkey started blocking connections to Tor? since yesterday i am only able to use Tor with bridges. the error message is "* connections died in state handshaking (TLS) with SSL state SSLv2/v3 read server hello A in HANDSHAKE"</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224956"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224956" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 13, 2016</p>
    </div>
    <a href="#comment-224956">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224956" class="permalink" rel="bookmark">Hi,
Are the official</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,</p>
<p>Are the official downloads time-stamped correctly?<br />
I have just had to rebuild a system, so was re-loading the Tor Browser and noticed the 01Jan 2000 time-stamp. Is that correct? Is this an anonymity thing?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224973"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224973" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 13, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224956" class="permalink" rel="bookmark">Hi,
Are the official</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224973">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224973" class="permalink" rel="bookmark">Yes, this is fine and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, this is fine and normal:<br />
<a href="https://www.torproject.org/docs/faq#Timestamps" rel="nofollow">https://www.torproject.org/docs/faq#Timestamps</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-224979"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224979" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 13, 2016</p>
    </div>
    <a href="#comment-224979">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224979" class="permalink" rel="bookmark">Tor Messenger will have</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Messenger will have option to run himself as server for XMPP?</p>
<p>Will there be a Tor IRC Client too?</p>
<p>What about a Tor hidden service bitcoin miner?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224981"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224981" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 13, 2016</p>
    </div>
    <a href="#comment-224981">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224981" class="permalink" rel="bookmark">When you think will be</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When you think will be stable release?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-225273"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225273" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">December 14, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224981" class="permalink" rel="bookmark">When you think will be</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-225273">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225273" class="permalink" rel="bookmark">That depends on what people</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That depends on what people find when they're testing the alpha torbrowser releases that just came out!  Unless there are new major bugs discovered, I'd expect to put out 0.2.9 stable some time towards the end of next week.  (No promises though)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-224982"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224982" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 13, 2016</p>
    </div>
    <a href="#comment-224982">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224982" class="permalink" rel="bookmark">There will be full changelog</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There will be full changelog in 0.2.9 stable release?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-225032"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225032" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 14, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224982" class="permalink" rel="bookmark">There will be full changelog</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-225032">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225032" class="permalink" rel="bookmark">There will be, yes.
In the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There will be, yes.</p>
<p>In the mean time you can read it all here:<br />
<a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=release-0.2.9" rel="nofollow">https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=release-0.2.9</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-224983"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224983" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 13, 2016</p>
    </div>
    <a href="#comment-224983">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224983" class="permalink" rel="bookmark">Can there be obfs4 included</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can there be obfs4 included in Tor.exe? Is hard to make bridge.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-225110"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225110" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 14, 2016</p>
    </div>
    <a href="#comment-225110">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225110" class="permalink" rel="bookmark">Is exit node IP ever planned</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is exit node IP ever planned to be hidden from public?</p>
<p>If website know you is use Tor and ISP.. is not really privacy.</p>
</div>
  </div>
</article>
<!-- Comment END -->
