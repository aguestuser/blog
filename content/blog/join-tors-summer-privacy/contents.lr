title: Join Tor's Summer of Privacy
---
pub_date: 2018-03-02
---
author: phoul
---
tags:

Tor Summer of Privacy
Summer of Privacy
internships
---
categories: internships
---
summary: We’ve got projects covering almost every part of Tor, from creating a Tor client in Python to stripping metadata from file uploads in Tor Browser. If none of our projects jump out at you -- get in touch with us and propose your own! If your project idea is accepted, we’ll mentor you and help bring it to life.
---
_html_body:

<p> </p>
<p>Back in 2014, we ran <a href="https://blog.torproject.org/tor-summer-privacy-apply-now">Tor Summer of Privacy</a>, a chance for developers to contribute code and help make Tor even stronger. Each coder was paired a core Tor developer who mentored them, provided guidance and encouragement, and helped with <a href="https://en.wikipedia.org/wiki/Rubber_duck_debugging">rubber-duck debugging</a>. </p>
<p>This year, in order to encourage developers to contribute to the world’s strongest privacy tool, we’re running Summer of Privacy again! The program is open to anyone, but the mentorship opportunities make it an especially good fit for students. After our <a href="https://blog.torproject.org/tor-outreachy-internships-underrepresented-people-tech">Outreachy</a> internships, this is the second paid opportunity to get involved with Tor so far this year.</p>
<h3><b>Use your skills </b></h3>
<p>We have a handful of projects in mind for this year. <a href="https://www.torproject.org/getinvolved/volunteer.html.en#Coding">Our volunteer page</a> has full details -- we’ve got projects covering almost every part of Tor, from creating a Tor client in Python to stripping metadata from file uploads in Tor Browser. If none of our projects jump out at you -- <a href="mailto:tor-sop@lists.torproject.org">get in touch with us</a> and propose your own! If your project idea is accepted, we’ll mentor you and help bring it to life.</p>
<p>Tor’s Summer of Privacy program matches Google’s Summer of Code <a href="https://developers.google.com/open-source/gsoc/help/student-stipends">student stipends</a>. </p>
<h3><b>Applying</b></h3>
<p>Applying for the Tor Summer of Privacy is as easy as sending a project proposal to our Summer of Privacy <a href="mailto:tor-sop@lists.torproject.org">mailing list</a>. If you’re looking for an idea of what proposals should look like, see <a href="https://www.torproject.org/about/gsoc.html.en#Example">these examples</a> of GSOC projects from previous years.  Our application phase officially begins on March 12th and ends on March 26th. We will announce the successful applications on April 20th.</p>
<p>So, what are you waiting for? :)</p>

---
_comments:

<a id="comment-274179"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274179" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="Osikhena Dania Abubakar">Osikhena Dania… (not verified)</span> said:</p>
      <p class="date-time">March 02, 2018</p>
    </div>
    <a href="#comment-274179">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274179" class="permalink" rel="bookmark">We need more African routers…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We need more African routers on the Tor Network , how can we make that happen?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274181"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274181" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 02, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274179" class="permalink" rel="bookmark">We need more African routers…</a> by <span content="Osikhena Dania Abubakar">Osikhena Dania… (not verified)</span></p>
    <a href="#comment-274181">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274181" class="permalink" rel="bookmark">do you mean by yourself or…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>do you mean by yourself or by an organization ?<br />
you must contact the mailing-list in both case.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274182"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274182" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  tommy
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">t0mmy</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">t0mmy said:</p>
      <p class="date-time">March 02, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274179" class="permalink" rel="bookmark">We need more African routers…</a> by <span content="Osikhena Dania Abubakar">Osikhena Dania… (not verified)</span></p>
    <a href="#comment-274182">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274182" class="permalink" rel="bookmark">You could run a rely in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You could run a rely in Africa, or encourage others to do the same. We just published a new guide detailing how to do so: <a href="https://blog.torproject.org/new-guide-running-tor-relay" rel="nofollow">https://blog.torproject.org/new-guide-running-tor-relay</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274184"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274184" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 02, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274179" class="permalink" rel="bookmark">We need more African routers…</a> by <span content="Osikhena Dania Abubakar">Osikhena Dania… (not verified)</span></p>
    <a href="#comment-274184">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274184" class="permalink" rel="bookmark">I second that: Africa is one…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I second that: Africa is one of the underserved continents where I think Tor could grow the user base (and improve geographic diversity, which helps everyone).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274186"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274186" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 02, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274179" class="permalink" rel="bookmark">We need more African routers…</a> by <span content="Osikhena Dania Abubakar">Osikhena Dania… (not verified)</span></p>
    <a href="#comment-274186">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274186" class="permalink" rel="bookmark">As someone from Africa: The…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As someone from Africa: The problem is that upload bandwidth is really low in general here, and the bandwidth is very expensive for someone to operate a Tor relay here, as well as the unknowns of how to deal with abuse and whether our ISPs will allow it. That said there are apparently some relays in South Africa.</p>
<p>Also if there's for example something like DigitalOcean that provides VPSs in some place in Africa then that can help as well.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274188"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274188" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 02, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274186" class="permalink" rel="bookmark">As someone from Africa: The…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274188">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274188" class="permalink" rel="bookmark">This probably is irrelevant,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This probably is irrelevant, but I hear there is actually very good cell phone coverage in Somalia and that most people there use cell phones for everything (paying for groceries, socializing when its unsafe to go out, etc).   So if there were some way to provide Tor via cell phones, we could increase the user base.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274189"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274189" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 02, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274188" class="permalink" rel="bookmark">This probably is irrelevant,…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274189">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274189" class="permalink" rel="bookmark">An open network using…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>An open network using cellphone or wifi (&amp; ir) is impossible &amp; Tor should be useless.<br />
Internet is a luxury product &amp; tor can run where the freedom of speech &amp; privacy are a stable fundamental right since a long time (not available in north africa, war_occupied zone, no-man-land). It seems that south africa could be a very good candidate.</p>
<p>is israel an african region ?<br />
they need promote trans, gay_lesbian, etc. movement (even the idiot &amp; handicapped are welcome) and accept them as a whole part of the community like do the u.s.a. : tor is a necessity.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274927"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274927" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Lou (not verified)</span> said:</p>
      <p class="date-time">April 17, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274188" class="permalink" rel="bookmark">This probably is irrelevant,…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274927">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274927" class="permalink" rel="bookmark">Yes, unfortunately many…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, unfortunately many people do not have cell phones which support Tor. They are optimized for battery life, not for processing power. The bandwidth is very limited, too, so Tor would consume a monthly allotment and leave nothing for the user :(</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-274180"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274180" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 02, 2018</p>
    </div>
    <a href="#comment-274180">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274180" class="permalink" rel="bookmark">nice  !…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>nice  !<br />
you are speaking about improving the network is not it ?<br />
if it is about TorBrowser , my firefox user.js is more hardened &amp; more secure than TB.<br />
- are you looking for a new concept of TB , a new skill in coding  ?<br />
- should it be possible that a TB (reproducible/verified) be adapted/tied at our personal user.js of firefox ? like that the users will use the same tor for the network (surfing) but some of us will have a second security layer (our user.js) for registering, posting, mailing ... is it not the case yet or was it not the case in a recent past ? privacy  without security is for teacher/pupil _ parent/children_tutor/prisoner : obsolete and vicious.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274185"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274185" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 02, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274180" class="permalink" rel="bookmark">nice  !…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274185">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274185" class="permalink" rel="bookmark">A related suggestion: a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A related suggestion: a security audit of any Tor product would always be welcome.  E.g Tor Browser would ideally be re-audited every two years, Tor Messenger might be nearing the point where it can be audited, etc.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274926"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274926" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Fio (not verified)</span> said:</p>
      <p class="date-time">April 17, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274180" class="permalink" rel="bookmark">nice  !…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274926">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274926" class="permalink" rel="bookmark">Please open a ticket on Trac…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please open a ticket on Trac (<a href="https://trac.torproject.org" rel="nofollow">https://trac.torproject.org</a>) and say what you did. Your improvements could help millions of people. Tor would really appreciate learning what you did.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274183"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274183" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 02, 2018</p>
    </div>
    <a href="#comment-274183">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274183" class="permalink" rel="bookmark">&gt; stripping metadata from…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; stripping metadata from file uploads in Tor Browser</p>
<p>A related and very valuable project would be to fix the shortcomings of mat, an easy to use command line application used in Tails which strips metadata from pdf, jpg, png etc.   At least it was supposed to.   The Tails devs were shocked to find that apparently mat didnt strip metadata from embedded images and embedded links in pdf documents that they carefully removed pdf from the file types supported by the Tails version of mat.  (From memory--- I might have some details wrong.)   So fixing the pdf routines in mat to strip metadata from embedded images and links would be very valuable, and might even meet the needs of the above cited project.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274187"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274187" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Hello (not verified)</span> said:</p>
      <p class="date-time">March 02, 2018</p>
    </div>
    <a href="#comment-274187">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274187" class="permalink" rel="bookmark">How will Tor Project deal…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am curious how the Tor Project and related may deal with the Fight Online Sex Trafficking Act especially since it is USA based and a long time target of tyrants and the "get an inch take a mile" colonialist?</p>
<p><a href="https://www.eff.org/deeplinks/2018/02/house-vote-fosta-win-censorship" rel="nofollow">https://www.eff.org/deeplinks/2018/02/house-vote-fosta-win-censorship</a></p>
<p>Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274193"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274193" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>MANUEL (not verified)</span> said:</p>
      <p class="date-time">March 02, 2018</p>
    </div>
    <a href="#comment-274193">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274193" class="permalink" rel="bookmark">I LIKE TO JOIN</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I LIKE TO JOIN</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274216"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274216" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 06, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274193" class="permalink" rel="bookmark">I LIKE TO JOIN</a> by <span>MANUEL (not verified)</span></p>
    <a href="#comment-274216">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274216" class="permalink" rel="bookmark">You can join by …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can join by </p>
<p> o downloading the appropriate (Windows, Linux, Mac) version of Tor Browser bundle</p>
<p>     <a href="https://www.torproject.org/download/download-easy.html.en" rel="nofollow">https://www.torproject.org/download/download-easy.html.en</a></p>
<p> o verifying the detached signature</p>
<p>     <a href="https://www.torproject.org/docs/verifying-signatures.html.en" rel="nofollow">https://www.torproject.org/docs/verifying-signatures.html.en</a></p>
<p> o installing TB by unpacking the tarball on your computer</p>
<p>     <a href="https://www.torproject.org/projects/torbrowser.html.en" rel="nofollow">https://www.torproject.org/projects/torbrowser.html.en</a></p>
<p> o using the start-tor-browser script to start browsing using Tor Browser (see the "Using Tor" section on the download page).</p>
<p>Alternatively, you can download Tails from Tails Project (an allied open source project)</p>
<p> <a href="https://tails.boum.org/getting_started/index.en.html" rel="nofollow">https://tails.boum.org/getting_started/index.en.html</a></p>
<p>or you can try these other Tor Project products:</p>
<p> <a href="https://www.torproject.org/projects/projects.html.en" rel="nofollow">https://www.torproject.org/projects/projects.html.en</a></p>
<p>If you like using Tor, it would be very good to make a contribution to ensure that Tor continues to be available in the future</p>
<p> <a href="https://donate.torproject.org/pdr" rel="nofollow">https://donate.torproject.org/pdr</a></p>
<p>If you have a server with suitable bandwidth, you can run your own Tor relay:</p>
<p> <a href="https://blog.torproject.org/new-guide-running-tor-relay" rel="nofollow">https://blog.torproject.org/new-guide-running-tor-relay</a></p>
<p>You can also volunteer your time/skills</p>
<p> <a href="https://www.torproject.org/getinvolved/volunteer.html.en" rel="nofollow">https://www.torproject.org/getinvolved/volunteer.html.en</a></p>
<p>You can request an email account here:</p>
<p> <a href="https://webmail.no-log.org/" rel="nofollow">https://webmail.no-log.org/</a><br />
    <a href="https://mail.riseup.net/" rel="nofollow">https://mail.riseup.net/</a><br />
    <a href="https://webmail.boum.org/" rel="nofollow">https://webmail.boum.org/</a></p>
<p>(But be aware that on at least one occasion, Riseup has been served with an NSL and was forced to comply after attempting to resist in a US Court; Riseup was forbidden from telling anyone about the NSL until the Court relented over the objections of an unknown government.  Further an agent of the CZ government attempted to use Hacking Team to put an implant on the Riseup mail server, as was revealed when a huge trove of HT emails were leaked (possibly by an employee who had grown concerned about HT's ugly client list).  Similar cautions hold for all pro-privacy websites--- you can expect the bad guys to try to infect users with malware.  For example, in 2012 GCHQ attempted to phish users in the #tor chatroom on IRC, as is described in their presentation which was leaked by Snowden.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274255"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274255" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 09, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274193" class="permalink" rel="bookmark">I LIKE TO JOIN</a> by <span>MANUEL (not verified)</span></p>
    <a href="#comment-274255">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274255" class="permalink" rel="bookmark">Welcome!  To join the Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Welcome!  To join the Tor community, please see</p>
<p><a href="https://blog.torproject.org/comment/273405#comment-273405" rel="nofollow">https://blog.torproject.org/comment/273405#comment-273405</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274232"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274232" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Colorado (not verified)</span> said:</p>
      <p class="date-time">March 07, 2018</p>
    </div>
    <a href="#comment-274232">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274232" class="permalink" rel="bookmark">I want to help. How do I run…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I want to help. How do I run a relay? Do I need to upgrade my Tor? Do I need to erase the previous version and download a new one? if so, which one? How long do I keep this running and how often? What will this do for Tor? Thanks, all:) Noob</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274233"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274233" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 07, 2018</p>
    </div>
    <a href="#comment-274233">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274233" class="permalink" rel="bookmark">Just want to once again…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just want to once again thank all Tor Project employees, node operators,  volunteers and users for providing and using Tor products.    </p>
<p>And to thank TP for using the word "privacy" in your motto.  Please never forget</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274276"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274276" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>chefarov (not verified)</span> said:</p>
      <p class="date-time">March 12, 2018</p>
    </div>
    <a href="#comment-274276">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274276" class="permalink" rel="bookmark">I have been trying to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have been trying to subscribe to the tor-sop list via <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-sop" rel="nofollow">https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-sop</a>, but moderator approval is still pending. I have already confirmed my email, however I don't get any emails back. Is the list dead?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276381"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276381" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>madhavan (not verified)</span> said:</p>
      <p class="date-time">August 16, 2018</p>
    </div>
    <a href="#comment-276381">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276381" class="permalink" rel="bookmark">i like  this browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i like  this browser</p>
</div>
  </div>
</article>
<!-- Comment END -->
