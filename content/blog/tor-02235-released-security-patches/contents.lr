title: Tor 0.2.2.35 is released (security patches)
---
pub_date: 2011-12-16
---
author: erinn
---
tags:

tor
security critical
security fixes
bug fixes
stable release
end of life
---
categories:

network
releases
---
_html_body:

<p><strong>Tor 0.2.2.35 fixes a critical heap-overflow security issue in Tor's<br />
  buffers code. Absolutely everybody should upgrade.</strong></p>

<p>  The bug relied on an incorrect calculation when making data continuous<br />
  in one of our IO buffers, if the first chunk of the buffer was<br />
  misaligned by just the wrong amount. The miscalculation would allow an<br />
  attacker to overflow a piece of heap-allocated memory. To mount this<br />
  attack, the attacker would need to either open a SOCKS connection to<br />
  Tor's SocksPort (usually restricted to localhost), or target a Tor<br />
  instance configured to make its connections through a SOCKS proxy<br />
  (which Tor does not do by default).</p>

<p>  Good security practice requires that all heap-overflow bugs should be<br />
  presumed to be exploitable until proven otherwise, so we are treating<br />
  this as a potential code execution attack. Please upgrade immediately!<br />
  This bug does not affect bufferevents-based builds of Tor. Special<br />
  thanks to "Vektor" for reporting this issue to us!</p>

<p>  Tor 0.2.2.35 also fixes several bugs in previous versions, including<br />
  crash bugs for unusual configurations, and a long-term bug that<br />
  would prevent Tor from starting on Windows machines with draconian<br />
  AV software.</p>

<p>  With this release, we remind everyone that 0.2.0.x has reached its<br />
  formal end-of-life. Those Tor versions have many known flaws, and<br />
  nobody should be using them. You should upgrade -- ideally to the<br />
  0.2.2.x series. If you're using a Linux or BSD and its packages are<br />
  obsolete, stop using those packages and upgrade anyway.</p>

<p>  The Tor 0.2.1.x series is also approaching its end-of-life: it will no<br />
  longer receive support after some time in early 2012.</p>

<p><a href="https://www.torproject.org/download" rel="nofollow">https://www.torproject.org/download</a></p>

<p><strong>Changes in version 0.2.2.35 - 2011-12-16</strong></p>

<p><strong>Major bugfixes:</strong>
</p>

<ul>
<li>Fix a heap overflow bug that could occur when trying to pull<br />
      data into the first chunk of a buffer, when that chunk had<br />
      already had some data drained from it. Fixes CVE-2011-2778;<br />
      bugfix on 0.2.0.16-alpha. Reported by "Vektor".</li>
<li>Initialize Libevent with the EVENT_BASE_FLAG_NOLOCK flag enabled, so<br />
      that it doesn't attempt to allocate a socketpair. This could cause<br />
      some problems on Windows systems with overzealous firewalls. Fix for<br />
      bug 4457; workaround for Libevent versions 2.0.1-alpha through<br />
      2.0.15-stable.</li>
<li>If we mark an OR connection for close based on a cell we process,<br />
      don't process any further cells on it. We already avoid further<br />
      reads on marked-for-close connections, but now we also discard the<br />
      cells we'd already read. Fixes bug 4299; bugfix on 0.2.0.10-alpha,<br />
      which was the first version where we might mark a connection for<br />
      close based on processing a cell on it.</li>
<li>Correctly sanity-check that we don't underflow on a memory<br />
     allocation (and then assert) for hidden service introduction<br />
      point decryption. Bug discovered by Dan Rosenberg. Fixes bug 4410;<br />
      bugfix on 0.2.1.5-alpha.</li>
<li>Fix a memory leak when we check whether a hidden service<br />
      descriptor has any usable introduction points left. Fixes bug<br />
      4424. Bugfix on 0.2.2.25-alpha.</li>
<li>Don't crash when we're running as a relay and don't have a GeoIP<br />
      file. Bugfix on 0.2.2.34; fixes bug 4340. This backports a fix<br />
      we've had in the 0.2.3.x branch already.</li>
<li>When running as a client, do not print a misleading (and plain<br />
      wrong) log message that we're collecting "directory request"<br />
      statistics: clients don't collect statistics. Also don't create a<br />
      useless (because empty) stats file in the stats/ directory. Fixes<br />
      bug 4353; bugfix on 0.2.2.34.</li>
</ul>

<p><strong>Minor bugfixes:</strong>
</p>

<ul>
<li>Detect failure to initialize Libevent. This fix provides better<br />
      detection for future instances of bug 4457.</li>
<li>Avoid frequent calls to the fairly expensive cull_wedged_cpuworkers<br />
      function. This was eating up hideously large amounts of time on some<br />
      busy servers. Fixes bug 4518; bugfix on 0.0.9.8.</li>
<li>Resolve an integer overflow bug in smartlist_ensure_capacity().<br />
      Fixes bug 4230; bugfix on Tor 0.1.0.1-rc. Based on a patch by<br />
      Mansour Moufid.</li>
<li>Don't warn about unused log_mutex in log.c when building with<br />
      --disable-threads using a recent GCC. Fixes bug 4437; bugfix on<br />
      0.1.0.6-rc which introduced --disable-threads.</li>
<li>When configuring, starting, or stopping an NT service, stop<br />
      immediately after the service configuration attempt has succeeded<br />
      or failed. Fixes bug 3963; bugfix on 0.2.0.7-alpha.</li>
<li>When sending a NETINFO cell, include the original address<br />
      received for the other side, not its canonical address. Found<br />
      by "troll_un"; fixes bug 4349; bugfix on 0.2.0.10-alpha.</li>
<li>Fix a typo in a hibernation-related log message. Fixes bug 4331;<br />
      bugfix on 0.2.2.23-alpha; found by "tmpname0901".</li>
<li>Fix a memory leak in launch_direct_bridge_descriptor_fetch() that<br />
      occurred when a client tried to fetch a descriptor for a bridge<br />
      in ExcludeNodes. Fixes bug 4383; bugfix on 0.2.2.25-alpha.</li>
<li>Backport fixes for a pair of compilation warnings on Windows.<br />
      Fixes bug 4521; bugfix on 0.2.2.28-beta and on 0.2.2.29-beta.</li>
<li>If we had ever tried to call tor_addr_to_str on an address of<br />
      unknown type, we would have done a strdup on an uninitialized<br />
      buffer. Now we won't. Fixes bug 4529; bugfix on 0.2.1.3-alpha.<br />
      Reported by "troll_un".</li>
<li>Correctly detect and handle transient lookup failures from<br />
      tor_addr_lookup. Fixes bug 4530; bugfix on 0.2.1.5-alpha.<br />
      Reported by "troll_un".</li>
<li>Fix null-pointer access that could occur if TLS allocation failed.<br />
      Fixes bug 4531; bugfix on 0.2.0.20-rc. Found by "troll_un".</li>
<li>Use tor_socket_t type for listener argument to accept(). Fixes bug<br />
      4535; bugfix on 0.2.2.28-beta. Found by "troll_un".</li>
</ul>

<p><strong>Minor features:</strong>
</p>

<ul>
<li>Add two new config options for directory authorities:<br />
      AuthDirFastGuarantee sets a bandwidth threshold for guaranteeing the<br />
      Fast flag, and AuthDirGuardBWGuarantee sets a bandwidth threshold<br />
      that is always sufficient to satisfy the bandwidth requirement for<br />
      the Guard flag. Now it will be easier for researchers to simulate<br />
      Tor networks with different values. Resolves ticket 4484.</li>
<li>When Tor ignores a hidden service specified in its configuration,<br />
      include the hidden service's directory in the warning message.<br />
      Previously, we would only tell the user that some hidden service<br />
      was ignored. Bugfix on 0.0.6; fixes bug 4426.</li>
<li>Update to the December 6 2011 Maxmind GeoLite Country database.</li>
</ul>

<p><strong>Packaging changes:</strong>
</p>

<ul>
<li>Make it easier to automate expert package builds on Windows,<br />
      by removing an absolute path from makensis.exe command.</li>
</ul>

---
_comments:

<a id="comment-13102"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13102" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2011</p>
    </div>
    <a href="#comment-13102">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13102" class="permalink" rel="bookmark">Thanks for this awesome</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for this awesome project</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13139"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13139" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2011</p>
    </div>
    <a href="#comment-13139">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13139" class="permalink" rel="bookmark">How can i update my tor ?
I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How can i update my tor ?<br />
I emailed you for new version 2011-12-18, but received the old version again!!!!!!<br />
the download page is so complicated!!!!!!!!! what are you doing ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13156"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13156" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2011</p>
    </div>
    <a href="#comment-13156">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13156" class="permalink" rel="bookmark">I went to the Tor Bundle and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I went to the Tor Bundle and downloaded and updated but it still says I am not using the latest version. I checked the settings on my Vidalia and it says I am using Tor 2.2.35...did I miss a step?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13165"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13165" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 21, 2011</p>
    </div>
    <a href="#comment-13165">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13165" class="permalink" rel="bookmark">Unfortunately, all your</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Unfortunately, all your 'torproject.org' links are NOT accessable i.e. have been blocked.</p>
<p>It says: </p>
<p>'Invalid Server Certificate<br />
You attempted to reach <a href="http://www.torproject.org" rel="nofollow">www.torproject.org</a>, but the server presented an invalid certificate.'</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-13168"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13168" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 21, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-13165" class="permalink" rel="bookmark">Unfortunately, all your</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-13168">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13168" class="permalink" rel="bookmark">Yep. Same here. TOR download</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yep. Same here. TOR download is having problems... Interesting and unusual, Do Not install TOR from another source or here until you hear why this happened.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14698"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14698" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-13165" class="permalink" rel="bookmark">Unfortunately, all your</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-14698">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14698" class="permalink" rel="bookmark">I get the &quot;You attempted to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I get the "You attempted to reach <a href="http://www.torproject.org" rel="nofollow">www.torproject.org</a>, but the server presented an invalid certificate." message too. </p>
<p>What the heck is going on</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-13229"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13229" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 25, 2011</p>
    </div>
    <a href="#comment-13229">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13229" class="permalink" rel="bookmark">New version installed,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>New version installed, message says it's running. But I can't open anything in Firefox,<br />
Error 504 everywhere. What did I do wrong?<br />
Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13485"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13485" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 10, 2012</p>
    </div>
    <a href="#comment-13485">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13485" class="permalink" rel="bookmark">I wante a new tor for my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I wante a new tor for my n900.</p>
</div>
  </div>
</article>
<!-- Comment END -->
