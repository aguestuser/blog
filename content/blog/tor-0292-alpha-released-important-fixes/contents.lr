title: Tor 0.2.9.2-alpha is released, with important fixes
---
pub_date: 2016-08-24
---
author: nickm
---
tags:

tor
alpha
release
---
categories:

network
releases
---
_html_body:

<p>Tor 0.2.9.2-alpha continues development of the 0.2.9 series with several new features and bugfixes. It also includes an important authority update and an important bugfix from 0.2.8.7. Everyone who sets the ReachableAddresses option, and all bridges, are strongly encouraged to upgrade to 0.2.8.7, or to 0.2.9.2-alpha.</p>

<p>You can download the source from the usual place on the website.<br />
Packages should be available over the next several days. Remember<br />
to check the signatures!<br />
Please note: This is an alpha release. You should only try this one if you are interested in tracking Tor development, testing new features, making sure that Tor still builds on unusual platforms, or generally trying to hunt down bugs. If you want a stable experience, please stick to the stable releases.<br />
Below are the changes since 0.2.9.1-alpha.</p>

<h2>Changes in version 0.2.9.2-alpha - 2016-08-24</h2>

<ul>
<li>Directory authority changes (also in 0.2.8.7):
<ul>
<li>The "Tonga" bridge authority has been retired; the new bridge authority is "Bifroest". Closes tickets 19728 and 19690.
  </li>
</ul>
</li>
<li>Major bugfixes (client, security, also in 0.2.8.7):
<ul>
<li>Only use the ReachableAddresses option to restrict the first hop in a path. In earlier versions of 0.2.8.x, it would apply to every hop in the path, with a possible degradation in anonymity for anyone using an uncommon ReachableAddress setting. Fixes bug <a href="https://bugs.torproject.org/19973" rel="nofollow">19973</a>; bugfix on 0.2.8.2-alpha.
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Major features (user interface):
<ul>
<li>Tor now supports the ability to declare options deprecated, so that we can recommend that people stop using them. Previously, this was done in an ad-hoc way. Closes ticket <a href="https://bugs.torproject.org/19820" rel="nofollow">19820</a>.
  </li>
</ul>
</li>
<li>Major bugfixes (directory downloads):
<ul>
<li>Avoid resetting download status for consensuses hourly, since we already have another, smarter retry mechanism. Fixes bug <a href="https://bugs.torproject.org/8625" rel="nofollow">8625</a>; bugfix on 0.2.0.9-alpha.
  </li>
</ul>
</li>
<li>Minor features (config):
<ul>
<li>Warn users when descriptor and port addresses are inconsistent. Mitigates bug <a href="https://bugs.torproject.org/13953" rel="nofollow">13953</a>; patch by teor.
  </li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the August 2 2016 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor features (user interface):
<ul>
<li>There is a new --list-deprecated-options command-line option to list all of the deprecated options. Implemented as part of ticket <a href="https://bugs.torproject.org/19820" rel="nofollow">19820</a>.
  </li>
</ul>
</li>
<li>Minor bugfixes (code style):
<ul>
<li>Fix an integer signedness conversion issue in the case conversion tables. Fixes bug <a href="https://bugs.torproject.org/19168" rel="nofollow">19168</a>; bugfix on 0.2.1.11-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Build correctly on versions of libevent2 without support for evutil_secure_rng_add_bytes(). Fixes bug <a href="https://bugs.torproject.org/19904" rel="nofollow">19904</a>; bugfix on 0.2.5.4-alpha.
  </li>
<li>Fix a compilation warning on GCC versions before 4.6. Our ENABLE_GCC_WARNING macro used the word "warning" as an argument, when it is also required as an argument to the compiler pragma. Fixes bug <a href="https://bugs.torproject.org/19901" rel="nofollow">19901</a>; bugfix on 0.2.9.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (compilation, also in 0.2.8.7):
<ul>
<li>Remove an inappropriate "inline" in tortls.c that was causing warnings on older versions of GCC. Fixes bug <a href="https://bugs.torproject.org/19903" rel="nofollow">19903</a>; bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (fallback directories, also in 0.2.8.7):
<ul>
<li>Avoid logging a NULL string pointer when loading fallback directory information. Fixes bug <a href="https://bugs.torproject.org/19947" rel="nofollow">19947</a>; bugfix on 0.2.4.7-alpha and 0.2.8.1-alpha. Report and patch by "rubiate".
  </li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>Log a more accurate message when we fail to dump a microdescriptor. Fixes bug <a href="https://bugs.torproject.org/17758" rel="nofollow">17758</a>; bugfix on 0.2.2.8-alpha. Patch from Daniel Pinto.
  </li>
</ul>
</li>
<li>Minor bugfixes (memory leak):
<ul>
<li>Fix a series of slow memory leaks related to parsing torrc files and options. Fixes bug <a href="https://bugs.torproject.org/19466" rel="nofollow">19466</a>; bugfix on 0.2.1.6-alpha.
  </li>
</ul>
</li>
<li>Deprecated features:
<ul>
<li>A number of DNS-cache-related sub-options for client ports are now deprecated for security reasons, and may be removed in a future version of Tor. (We believe that client-side DNS cacheing is a bad idea for anonymity, and you should not turn it on.) The options are: CacheDNS, CacheIPv4DNS, CacheIPv6DNS, UseDNSCache, UseIPv4Cache, and UseIPv6Cache.
  </li>
<li>A number of options are deprecated for security reasons, and may be removed in a future version of Tor. The options are: AllowDotExit, AllowInvalidNodes, AllowSingleHopCircuits, AllowSingleHopExits, ClientDNSRejectInternalAddresses, CloseHSClientCircuitsImmediatelyOnTimeout, CloseHSServiceRendCircuitsImmediatelyOnTimeout, ExcludeSingleHopRelays, FastFirstHopPK, TLSECGroup, UseNTorHandshake, and WarnUnsafeSocks.
  </li>
<li>The *ListenAddress options are now deprecated as unnecessary: the corresponding *Port options should be used instead. These options may someday be removed. The affected options are: ControlListenAddress, DNSListenAddress, DirListenAddress, NATDListenAddress, ORListenAddress, SocksListenAddress, and TransListenAddress.
  </li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Correct the IPv6 syntax in our documentation for the VirtualAddrNetworkIPv6 torrc option. Closes ticket <a href="https://bugs.torproject.org/19743" rel="nofollow">19743</a>.
  </li>
</ul>
</li>
<li>Removed code:
<ul>
<li>We no longer include the (dead, deprecated) bufferevent code in Tor. Closes ticket <a href="https://bugs.torproject.org/19450" rel="nofollow">19450</a>. Based on a patch from U+039b.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-203024"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-203024" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 25, 2016</p>
    </div>
    <a href="#comment-203024">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-203024" class="permalink" rel="bookmark">i am sorry to say that it</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i am sorry to say that it doesn´t work in mac books!</p>
<p>keep trying</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-203176"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-203176" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 26, 2016</p>
    </div>
    <a href="#comment-203176">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-203176" class="permalink" rel="bookmark">Why does Atlas show two</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why does Atlas show two relays with the 'Bifroest' name - <a href="https://atlas.torproject.org/#details/1D8F3A91C37C5D1C4C19B1AD1D0CFBE8BF72D8E1" rel="nofollow">https://atlas.torproject.org/#details/1D8F3A91C37C5D1C4C19B1AD1D0CFBE8B…</a> and <a href="https://atlas.torproject.org/#details/0BFC92A77CC633B0D8FBE1D743C1B6960542CB84" rel="nofollow">https://atlas.torproject.org/#details/0BFC92A77CC633B0D8FBE1D743C1B6960…</a></p>
<p>?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-203308"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-203308" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 27, 2016</p>
    </div>
    <a href="#comment-203308">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-203308" class="permalink" rel="bookmark">Has this 0.2.9.2 version</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Has this 0.2.9.2 version severely broken access to hidden services ? Any connection attempt to dot.onions seem to fail - also, failed attempts not logged to console any more ! Everything was still working as usual under 0.2.9.1 - config NOT modified (Windows XP)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-203309"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-203309" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 27, 2016</p>
    </div>
    <a href="#comment-203309">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-203309" class="permalink" rel="bookmark">Re : comment about onions</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Re : comment about onions not working. Scratch that ! I can still access the Duckduckgo hidden service. Most others unavailable (???) Still, failures and timeouts wrt/ hidden onions not appearing intje log any more.</p>
</div>
  </div>
</article>
<!-- Comment END -->
