title: Tor Weekly News — May 28th, 2014
---
pub_date: 2014-05-28
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-first issue of Tor Weekly News in 2014, the weekly newsletter that covers what is happening in the Tor community.</p>

<h1>OnionShare and tor’s ControlPort</h1>

<p>Micah Lee published <a href="https://github.com/micahflee/onionshare" rel="nofollow">OnionShare</a>, a program that “makes it simple to share a file securely using a password-protected Tor hidden service”. It originally ran only in Tails, but has now been made compatible with other GNU/Linux distros, Windows, and OS X. As part of that process, Micah <a href="https://lists.torproject.org/pipermail/tor-dev/2014-May/006895.html" rel="nofollow">wondered</a> about the best way to make the program work with a Tor Browser or system tor process, as “I would really like to not be in the business of distributing Tor myself”. <a href="https://lists.torproject.org/pipermail/tor-dev/2014-May/006896.html" rel="nofollow">meejah</a> and <a href="https://lists.torproject.org/pipermail/tor-dev/2014-May/006899.html" rel="nofollow">David Stainton</a> responded with relevant details of the <a href="https://stem.torproject.org/" rel="nofollow">Stem</a> and <a href="https://github.com/meejah/txtorcon" rel="nofollow">txtorcon</a> controller libraries, which allow this kind of operation to take place via tor’s ControlPort.</p>

<h1>The “Tor and HTTPS” visualization made translatable</h1>

<p>Lunar <a href="https://lists.torproject.org/pipermail/tor-talk/2014-May/033001.html" rel="nofollow">announced</a> the creation of a <a href="https://people.torproject.org/~lunar/tor-and-https/" rel="nofollow">repository</a> for an SVG+Javascript version of the EFF’s interactive <a href="https://www.eff.org/pages/tor-and-https/" rel="nofollow">“Tor and HTTPS” visualization</a>, which has proven useful in explaining to users the types of data that can be leaked or intercepted, and by whom, when using Tor or HTTPS (or both, or neither). As Lunar wrote, “The good news is that it’s translatable”: copies have so far been published in over twenty languages. The amount of translation required is very small, so if you’d like to contribute in your language then download the <a href="https://gitweb.torproject.org/user/lunar/tor-and-https.git/blob/HEAD:/tor-and-https.pot" rel="nofollow">POT file</a> and submit a patch!</p>

<h1>A Child’s Garden of Pluggable Transports</h1>

<p>David Fifield <a href="https://lists.torproject.org/pipermail/tor-dev/2014-May/006891.html" rel="nofollow">published</a> <a href="https://trac.torproject.org/projects/tor/wiki/doc/AChildsGardenOfPluggableTransports" rel="nofollow">“A Child’s Garden of Pluggable Transports”</a>, a detailed visualization of different pluggable transport protocols, including “aspects of different transports that I think are hard to intuit, such as what flash proxy rendezvous looks like, and how transports look under the encrypted layer that is visible to a censor”. A few <a href="https://www.torproject.org/docs/pluggable-transports" rel="nofollow">other transports supported by Tor</a> are not yet discussed in the guide; “if you know how to run any of those transports, and you know an effective way to visualize it, please add it to the page”, wrote David.</p>

<h1>Miscellaneous news</h1>

<p>Anthony G. Basile <a href="http://opensource.dyc.edu/pipermail/tor-ramdisk/2014-May/000131.html" rel="nofollow">released</a> version 20140520 of <a href="http://opensource.dyc.edu/tor-ramdisk" rel="nofollow">tor-ramdisk</a>, the micro Linux distribution “whose only purpose is to host a Tor server in an environment that maximizes security and privacy”. The new version upgrades Tor to version 0.2.4.22, which “adds an important block to authority signing keys that were used on authorities vulnerable to the “heartbleed” bug in OpenSSL”, among other fixes; upgrading “is strongly recommended”.</p>

<p>Cure53 <a href="https://cure53.de/pentest-report_onion-browser.pdf" rel="nofollow">audited the security</a> of the <a href="https://mike.tig.as/onionbrowser/" rel="nofollow">Onion Browser</a>, a web browser for iOS platforms tunneling traffic through Tor. From the conclusion: “we believe that the Onion Browser project is on the right track, however there is still a long way ahead for the project to be appropriately ‘ripe’ for usage in actually privacy-relevant and critically important scenarios.” All reported issues should have been fixed in <a href="https://mike.tig.as/onionbrowser/security/#v1_5" rel="nofollow">release 1.5</a> on May 14th.</p>

<p>A new pluggable transport, currently named <a href="https://github.com/Yawning/obfs4" rel="nofollow">obfs4</a>, is being crafted by Yawning Angel: “obfs4 is ScrambleSuit with djb crypto. Instead of obfs3 style UniformDH and CTR-AES256/HMAC-SHA256, obfs4 uses a combination of Curve25519, Elligator2, HMAC-SHA256, XSalsa20/Poly1305 and SipHash-2-4”. The feature set offered by obfs4 is comparable to ScrambleSuit, with minor differences. Yawning is now asking the community for <a href="https://lists.torproject.org/pipermail/tor-dev/2014-May/006897.html" rel="nofollow">comments, reviews, and tests</a>.</p>

<p>Stem now offers a <a href="https://blog.torproject.org/blog/new-feature-tor-interpreter" rel="nofollow">control interpreter</a>, “a new method for interacting with Tor’s control interface that combines an interactive python interpreter with raw access similar to telnet”. Damian Johnson wrote a <a href="https://stem.torproject.org/tutorials/down_the_rabbit_hole.html" rel="nofollow">new tutorial</a> to give an overview of what can be done with it.</p>

<p>Also on the controller front, Yawning Angel hacked on <a href="https://github.com/yawning/or-applet" rel="nofollow">or-applet</a>, a Gtk+ system tray applet to monitor Tor circuits.</p>

<p>Arlo Breault is making progress on the Tor Instant Messenger Bundle: a minimalistic <a href="https://bugs.torproject.org/11533" rel="nofollow">user interface for OTR encryption in Instantbird</a>, one of the key features missing from the finished software, has now been implemented.</p>

<p>Nicolas Vigier has been <a href="https://lists.torproject.org/pipermail/tor-dev/2014-May/006911.html" rel="nofollow">working</a> on improving the <a href="https://github.com/tsgates/mbox/" rel="nofollow">Mbox sandboxing environment</a> to test the Tor Browser for disk or network leaks.</p>

<p>Israel Leiva <a href="https://lists.torproject.org/pipermail/tor-dev/2014-May/006903.html" rel="nofollow">published</a> the initial version of a <a href="https://github.com/ileiva/gettor/blob/master/spec/overview.txt" rel="nofollow">design proposal</a> for the “Revamp GetTor” Google Summer of Code project, having concluded that a full rewrite is needed.</p>

<p>Juha Nurmi submitted the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-May/000536.html" rel="nofollow">first weekly report</a> for the ahmia.fi GSoC project.</p>

<p>kzhm <a href="https://lists.torproject.org/pipermail/tor-talk/2014-May/033032.html" rel="nofollow">sent out</a> instructions for installing obfsproxy on Fedora 20, to go with <a href="https://www.torproject.org/projects/obfsproxy-instructions.html" rel="nofollow">those for other Linux distributions</a>.</p>

<p><a href="https://code.google.com/p/address-sanitizer/wiki/AddressSanitizer" rel="nofollow">AddressSanitizer</a> (ASan) is a powerful memory error detector: software built with such technology makes it a lot harder to exploit programming errors related to memory management. Happily, Georg Koppen has <a href="https://lists.torproject.org/pipermail/tor-qa/2014-May/000414.html" rel="nofollow">announced</a> the first test packages of the Tor Browser built with ASan hardening.</p>

<p>Karsten Loesing is planning on <a href="https://lists.torproject.org/pipermail/tor-dev/2014-May/006909.html" rel="nofollow">spinning off the directory archive from the metrics portal</a>.</p>

<h1>Tor help desk roundup</h1>

<p>Multiple Mac OS X users complained that despite seeing the “Congratulations” welcome page, they were unable to reach any website with the Tor Browser. It appears that with a recent update, the Sophos anti-virus solution interferes with the Tor Browser. In order to be able to use the Tor Browser again, one must open Sophos Anti-Virus, then “Preferences”, and in the “Web Protection” panel position all switches to off.</p>

<h1>News from Tor StackExchange</h1>

<p>yohann2008 doesn’t want their hidden service to be <a href="https://tor.stackexchange.com/q/2130/88" rel="nofollow">indexed by search engines</a>. puser suggested using a <a href="https://en.wikipedia.org/wiki/Robots_exclusion_standard" rel="nofollow">robots.txt file</a>, as on a normal webpage. Jens Kubieziel later received confirmation on the IRC channel of <a href="https://ahmia.fi/" rel="nofollow">ahmia.fi</a> that this search engine does indeed respect the robots.txt; however, it is unknown whether others do.</p>

<p>Herbalist saw the <a href="https://tor.stackexchange.com/q/1866/88" rel="nofollow">following line in their log file</a> and wonders what it could mean: “Rejecting INTRODUCE1 on non-OR or non-edge circuit 7503”. If you can unravel this mystery, please submit your answer to the question.</p>

<h1>Easy development tasks to get involved with</h1>

<p>The metrics website displays graphs on <a href="https://metrics.torproject.org/users.html#userstats-bridge-transport" rel="nofollow">bridge users by pluggable transport</a>, but we’d like to have another graph with <a href="https://bugs.torproject.org/11799" rel="nofollow">total pluggable transport usage</a>. Karsten Loesing outlined the steps for adding such a graph, which require some knowledge of R and ggplot2. If you enjoy writing R and want to add this new graph to the metrics website, give it a try and post your results on the ticket.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, harmony, qbi, and Karsten Loesing.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

