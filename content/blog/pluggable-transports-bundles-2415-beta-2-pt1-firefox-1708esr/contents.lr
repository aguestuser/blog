title: Pluggable transports bundles 2.4.15-beta-2-pt1 with Firefox 17.0.8esr
---
pub_date: 2013-08-11
---
author: dcf
---
tags:

obfsproxy
pluggable transports
flashproxy
---
categories: circumvention
---
_html_body:

<p>We've updated the Pluggable Transports Tor Browser Bundles with Firefox 17.0.8esr and Tor 0.2.4.15-beta-2. These correspond to the <a href="https://blog.torproject.org/blog/new-tor-browser-bundles-firefox-1708esr" rel="nofollow">Tor Browser Bundle release</a> of August 9 and contain important security fixes.</p>

<ul>
<li><a href="https://www.torproject.org/dist/torbrowser/tor-pluggable-transports-browser-2.4.15-beta-2-pt1_en-US.exe" rel="nofollow">Windows</a> (<a href="https://www.torproject.org/dist/torbrowser/tor-pluggable-transports-browser-2.4.15-beta-2-pt1_en-US.exe.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://www.torproject.org/dist/torbrowser/osx/TorBrowser-Pluggable-Transports-2.4.15-beta-2-pt1-osx-i386-en-US.zip" rel="nofollow">Mac OS X</a> (<a href="https://www.torproject.org/dist/torbrowser/osx/TorBrowser-Pluggable-Transports-2.4.15-beta-2-pt1-osx-i386-en-US.zip.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://www.torproject.org/dist/torbrowser/linux/tor-pluggable-transports-browser-gnu-linux-i686-2.4.15-beta-2-pt1-dev-en-US.tar.gz" rel="nofollow">GNU/Linux 32-bit</a> (<a href="https://www.torproject.org/dist/torbrowser/linux/tor-pluggable-transports-browser-gnu-linux-i686-2.4.15-beta-2-pt1-dev-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://www.torproject.org/dist/torbrowser/linux/tor-pluggable-transports-browser-gnu-linux-x86_64-2.4.15-beta-2-pt1-dev-en-US.tar.gz" rel="nofollow">GNU/Linux 64-bit</a> (<a href="https://www.torproject.org/dist/torbrowser/linux/tor-pluggable-transports-browser-gnu-linux-x86_64-2.4.15-beta-2-pt1-dev-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
</ul>

<p>These bundles contain flash proxy and obfsproxy configured to run by default. If you want to use flash proxy, you will have to take the extra steps listed in the <a href="https://trac.torproject.org/projects/tor/wiki/FlashProxyHowto" rel="nofollow">flash proxy howto</a>.</p>

<p>These bundles contain the same hardcoded obfs2 bridge addresses as the previous bundles which may work for some jurisdictions but you are strongly advised to get new bridge addresses from <a href="https://bridges.torproject.org" rel="nofollow">BridgeDB</a>.</p>

<p>These bundles are signed by David Fifield (0x5CD388E5) with <a href="https://crypto.stanford.edu/flashproxy/#verify-sig" rel="nofollow">this fingerprint</a>.</p>

---
_comments:

<a id="comment-33518"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33518" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 11, 2013</p>
    </div>
    <a href="#comment-33518">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33518" class="permalink" rel="bookmark">Where is the FreeBSD build?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Where is the FreeBSD build? I'd really prefer to avoid using poorly and insecurely  coded kernels when there are better options available.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-33563"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33563" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 11, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-33518" class="permalink" rel="bookmark">Where is the FreeBSD build?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-33563">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33563" class="permalink" rel="bookmark">I hear FreeBSD can run Linux</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I hear FreeBSD can run Linux programs if you try hard enough.</p>
<p>Otherwise, the answer is "why haven't you made one for us yet?"</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-33545"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33545" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 11, 2013</p>
    </div>
    <a href="#comment-33545">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33545" class="permalink" rel="bookmark">China here. Update works</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>China here. Update works fine. This is great.<br />
Thank you very much.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-33712"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33712" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 15, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-33545" class="permalink" rel="bookmark">China here. Update works</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-33712">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33712" class="permalink" rel="bookmark">can not run new plugged</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>can not run new plugged bridge in china</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-33582"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33582" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 12, 2013</p>
    </div>
    <a href="#comment-33582">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33582" class="permalink" rel="bookmark">Shazam! Thank ya, thank ya,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Shazam! Thank ya, thank ya, thank ya!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-33616"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33616" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 12, 2013</p>
    </div>
    <a href="#comment-33616">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33616" class="permalink" rel="bookmark">What are the cool new</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What are the cool new features in 2.4? ECDSA?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-33625"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33625" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 13, 2013</p>
    </div>
    <a href="#comment-33625">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33625" class="permalink" rel="bookmark">I just installed the OS X</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I just installed the OS X version but the green onion keeps blinking, saying I should update.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-33653"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33653" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 13, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-33625" class="permalink" rel="bookmark">I just installed the OS X</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-33653">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33653" class="permalink" rel="bookmark">I think I have witnessed the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think I have witnessed the same behaviour on my linux machine earlier today. I downloaded it again just some minutes ago and this time I don't get the message. So my advice is to download it again, probably there was an update some hours ago.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-33651"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33651" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 13, 2013</p>
    </div>
    <a href="#comment-33651">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33651" class="permalink" rel="bookmark">Thank you!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-33707"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33707" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 14, 2013</p>
    </div>
    <a href="#comment-33707">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33707" class="permalink" rel="bookmark">From Iran:
For the first</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>From Iran:<br />
For the first time the connecting time for "Pluggable transports bundles 2.4.15-beta-2-pt1 with Firefox 17.0.8esr" is about 20 sec.<br />
But the next time it length about 3 min or more.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-33732"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33732" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 16, 2013</p>
    </div>
    <a href="#comment-33732">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33732" class="permalink" rel="bookmark">2.4.16-beta-1 on the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>2.4.16-beta-1 on the Pluggable Transports page. 2.4.15-beta-2 on the Blog page. Which one is correct?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-33884"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33884" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 23, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-33732" class="permalink" rel="bookmark">2.4.16-beta-1 on the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-33884">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33884" class="permalink" rel="bookmark">The newer one. It has Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The newer one. It has Tor 0.2.4.16-rc, which is better.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-33810"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33810" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 21, 2013</p>
    </div>
    <a href="#comment-33810">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33810" class="permalink" rel="bookmark">It seems BridgeDB is no</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It seems BridgeDB is no longer giving obfs proxy addresses?<br />
the plain bridges work here in China... for a few minutes...<br />
but its only giving 3 plain bridges, each repeated 3 times :P<br />
and the obfs addresses i have from before are dying out 1 by one.</p>
<p><div class="geshifilter"><pre class="php geshifilter-php" style="font-family:monospace;"><ol><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Aug xx xx<span style="color: #339933;">:</span><span style="color: #cc66cc;">28</span><span style="color: #339933;">:</span><span style="color: #208080;">04</span><span style="color: #339933;">.</span>xxx <span style="color: #009900;">&#91;</span>Warning<span style="color: #009900;">&#93;</span> Problem bootstrapping<span style="color: #339933;">.</span> Stuck at <span style="color: #cc66cc;">90</span><span style="color: #339933;">%:</span> Establishing a Tor circuit<span style="color: #339933;">.</span> <span style="color: #009900;">&#40;</span>Connection timed out <span style="color: #009900;">&#91;</span>WSAETIMEDOUT <span style="color: #009900;">&#93;</span><span style="color: #339933;">;</span> TIMEOUT<span style="color: #339933;">;</span> <a href="http://www.php.net/count"><span style="color: #990000;">count</span></a> <span style="color: #cc66cc;">20</span><span style="color: #339933;">;</span> recommendation warn<span style="color: #009900;">&#41;</span><span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Aug xx xx<span style="color: #339933;">:</span><span style="color: #cc66cc;">28</span><span style="color: #339933;">:</span><span style="color: #208080;">04</span><span style="color: #339933;">.</span>xxx <span style="color: #009900;">&#91;</span>Warning<span style="color: #009900;">&#93;</span> <span style="color: #cc66cc;">19</span> connections have failed<span style="color: #339933;">:&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Aug xx xx<span style="color: #339933;">:</span><span style="color: #cc66cc;">28</span><span style="color: #339933;">:</span><span style="color: #208080;">04</span><span style="color: #339933;">.</span>xxx <span style="color: #009900;">&#91;</span>Warning<span style="color: #009900;">&#93;</span> <span style="color: #cc66cc;">19</span> connections died in state connect<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span>ing with SSL state <span style="color: #009900;">&#40;</span>No SSL object<span style="color: #009900;">&#41;</span></div></li></ol></pre></div></p>
<p>Ummm which 19 exactly?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-33997"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33997" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 27, 2013</p>
    </div>
    <a href="#comment-33997">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33997" class="permalink" rel="bookmark">Seems there are no obfsproxy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Seems there are no obfsproxy is given in the BridgeDB, which are harder to block at least in Iran</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-34222"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-34222" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 05, 2013</p>
    </div>
    <a href="#comment-34222">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-34222" class="permalink" rel="bookmark">九月 05 22:16:59.031</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>九月 05 22:16:59.031 [Notice] Tor v0.2.4.16-rc (git-889e9bd529297284) running on Windows XP with Libevent 2.0.21-stable and OpenSSL 1.0.0k.<br />
九月 05 22:16:59.031 [Notice] Tor can't help you if you use it wrong! Learn how to be safe at <a href="https://www.torproject.org/download/download#warning" rel="nofollow">https://www.torproject.org/download/download#warning</a><br />
九月 05 22:16:59.140 [Notice] Read configuration file "C:\Proxy\tor-pluggable-transports-browser-2.4.16-beta-1-pt1_en-US\Data\Tor\torrc".<br />
九月 05 22:16:59.140 [Notice] Opening Socks listener on 127.0.0.1:9150<br />
九月 05 22:16:59.140 [Notice] Opening Control listener on 127.0.0.1:9151<br />
九月 05 22:16:59.140 [Notice] Parsing GEOIP IPv4 file .\Data\Tor\geoip.<br />
九月 05 22:16:59.359 [Notice] Parsing GEOIP IPv6 file .\Data\Tor\geoip6.<br />
九月 05 22:17:06.015 [Notice] Bootstrapped 5%: Connecting to directory server.<br />
九月 05 22:17:06.015 [Notice] Bootstrapped 10%: Finishing handshake with directory server.<br />
……</p>
<p>九月 05 22:18:35.218 [Notice] Tor has successfully opened a circuit. Looks like client functionality is working.<br />
九月 05 22:18:35.218 [Notice] Bootstrapped 100%: Done.<br />
九月 05 22:18:36.859 [Warning] onion_skin_client_handshake failed.<br />
九月 05 22:18:36.859 [Warning] circuit_finish_handshake failed.<br />
九月 05 22:18:36.859 [Warning] connection_edge_process_relay_cell (at origin) failed.<br />
九月 05 22:19:06.171 [Warning] onion_skin_client_handshake failed.<br />
九月 05 22:19:06.171 [Warning] circuit_finish_handshake failed.<br />
九月 05 22:19:06.171 [Warning] connection_edge_process_relay_cell (at origin) failed.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-36436"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-36436" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 20, 2013</p>
    </div>
    <a href="#comment-36436">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-36436" class="permalink" rel="bookmark">We need all this develpment</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We need all this develpment for Chorme also.</p>
</div>
  </div>
</article>
<!-- Comment END -->
